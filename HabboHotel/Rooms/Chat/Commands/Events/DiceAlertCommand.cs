﻿using Cloud.Communication.Packets.Outgoing.Notifications;
using Cloud.HabboHotel.GameClients;
using Cloud.Core;

namespace Cloud.HabboHotel.Rooms.Chat.Commands.Events
{
   class DiceAlertCommand : IChatCommand
    {
        public string PermissionRequired => "command_da2_alert";
        public string Parameters => "[MENSAJE]";
        public string Description => "Enviar una alerta de hotel para su evento!";

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            if (Session == null) return;
            if (Room == null) return;
            CloudServer.GetGame().GetClientManager().SendMessage(new SuperNotificationComposer(NotificationSettings.NOTIFICATION_OLE_IMG, "¡Se han abierto los dados oficiales!", "El inter que abre los dados es: <b><font color='#FF8000'>" + Session.GetHabbo().Username + " </font></b>\nA diferencia de los dados comunes, es que en estos puedes apostar con total seguridad" + "\r\rLos inters serán los encargados de supervisar que todo se realiza de manera correcta\n\n ¡¿A QUE ESPERAS?! ¡Ven ya y gana apostando contra otros usuarios!",
                "Ir a la sala", "event:navigator/goto/" + Room.Id));
        }
    }
}