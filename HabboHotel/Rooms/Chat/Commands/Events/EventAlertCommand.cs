﻿using Cloud.Communication.Packets.Outgoing.Rooms.Notifications;
using Cloud.HabboHotel.GameClients;
using Cloud.Core;

namespace Cloud.HabboHotel.Rooms.Chat.Commands.Events
{
    class EventAlertCommand : IChatCommand
    {
        public string PermissionRequired => "command_event_alert";
        public string Parameters => "[MENSAJE]";
        public string Description => "Enviar Alerta de Evento al Hotel!";

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            
            if (Session != null)
            {
                if (Room != null)
                {
                    if (Params.Length == 1)
                    {
                        Session.SendWhisper("Por favor, digite un mensaje para enviar.");
                        return;
                    }
                    else
                    {
                        string Message = CommandManager.MergeParams(Params, 1);

                        CloudServer.GetGame().GetClientManager().SendMessage(new RoomNotificationComposer("¡Nuevo evento en "+CloudServer.HotelName+"!",
                             "¡<font color=\"#00adff\"> <b>" + Session.GetHabbo().Username + "</b></font> está organizando un nuevo evento en este momento! Si quieres ganar  <font color='#ff8f00'><b>" + ExtraSettings.PTOS_COINS + "</b></font> participa ahora mismo.<br><br>" +
                             "¿Quieres participar en este juego? ¡Haz click en el botón inferior de  <b>Ir a la sala del evento</b>, y dentro podrás participar, sigue las instrucciones!<br><br>" +
                             "¿De qué trata este evento?<br><br>"+
                             "<font color=\"#f11648\"><b>" + Message + "</b></font><br><br>"+
                             "¡Te esperamos con los brazos abiertos, " + CloudServer.HotelName + "!",
                             NotificationSettings.NOTIFICATION_EVENT_IMG, "¡Ir a la sala del evento!", "event:navigator/goto/" + Session.GetHabbo().CurrentRoomId));
                    }
                }
            }
        }

    }
}