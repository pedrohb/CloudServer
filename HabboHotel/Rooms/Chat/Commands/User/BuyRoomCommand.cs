﻿using Cloud.Communication.Packets.Outgoing.Inventory.Purse;
using Cloud.Database.Interfaces;
using Cloud.HabboHotel.GameClients;
using System.Collections.Generic;
using System.Linq;
using Cloud.Communication.Packets.Outgoing.Rooms.Session;

namespace Cloud.HabboHotel.Rooms.Chat.Commands.User
{
    class BuyRoomCommand : IChatCommand
    {
        public string Description => "Compra una sala en venta de cualquier usuario.";
        public string Parameters => "";
        public string PermissionRequired => "command_buy_room";

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            RoomUser Owner = Room.GetRoomUserManager().GetRoomUserByHabbo(Room.RoomData.OwnerId);

            if (User == null)
                return;

            if (!Room.RoomForSale)
            {
                Session.SendWhisper("Esta sala no se encuentra en venta, contacta con su dueño si te interesa:" + Room.OwnerName);
                return;
            }

            if (Room.OwnerId == User.HabboId)
            {
                Session.SendWhisper("No puedes comprar tu propia sala.");
                return;
            }

            if (User.GetClient().GetHabbo().Duckets >= Room.ForSaleAmount)
            {
                Room.AssignNewOwner(Room, User, Owner);
            }
            else
            {
                User.GetClient().SendWhisper("¡No tienes suficientes duckets!");
                return;
            }

        }
    }
}
