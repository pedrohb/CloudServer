﻿using System;
using Cloud.Core;
using Cloud.HabboHotel.GameClients;
using Cloud.Communication.Packets.Outgoing.Rooms.Notifications;

namespace Cloud.HabboHotel.Rooms.Chat.Commands.User
{
    class InfoCommand : IChatCommand
    {
        public string PermissionRequired => "command_pickall";
        public string Parameters => "";
        public string Description => "Muestra información del servidor.";

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            TimeSpan Uptime = DateTime.Now - CloudServer.ServerStarted;
            int OnlineUsers = CloudServer.GetGame().GetClientManager().Count;
            int RoomCount = CloudServer.GetGame().GetRoomManager().Count;

            Session.SendMessage(new RoomNotificationComposer("Genesis Emulator " + CloudServer.PrettyBuild +" "+ CloudServer.VersionCloud + ":",
                 "<font color=\"#2196F3\"><b>Genesis Emulator:</b></font>\n" +
                 "<font size=\"11\" color=\"#1C1C1C\">Genesis Emulator para " + CloudServer.HotelName + " </font>" +
                 "<font size=\"11\" color=\"#1C1C1C\">el cual emula las funciones basicas de habbo para nuestros usuarios, añadiendo un poco de contenido propio.</font>\n\n" +
                 "<font color=\"#2196F3\" size=\"13\"><b>Estadísticas:</b></font>\n" +
                 "<font size=\"11\" color=\"#1C1C1C\">  <b> · Usuarios: </b> " + OnlineUsers + "</font>\n" +
                 "<font size=\"11\" color=\"#1C1C1C\">  <b> · Salas: </b> " + RoomCount + "</font>\n" +
                 "<font size=\"11\" color=\"#1C1C1C\">  <b> · Tiempo: </b> " + Uptime.Days + " día(s), " + Uptime.Hours + " horas & " + Uptime.Minutes + " minutos.</font>\n" +
                 "<font size=\"11\" color=\"#1C1C1C\">  <b> · Fecha: </b> " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + "</font>\n" +
                 "<font size=\"11\" color=\"#1C1C1C\">  <b> · Record: </b>  " + Game.SessionUserRecord + "</font>\n\n" +
                 "<font color=\"#2196F3\">Licencia:  <b>" + CloudServer.Licenseto + "</b></font>\n\n" +
                 "<font color=\"#2196F3\" size=\"13\"><b>Últimas Características:</b></font>\n" +
                 "<font color=\"#1C1C1C\" size=\"9\"> · EVENTOS ESPECIALES.</font>\n" +
                 "<font color=\"#1C1C1C\" size=\"9\"> · HUEVOS/COFRES MAGICOS.</font>\n" +
                 "<font color=\"#1C1C1C\" size=\"9\"> · INTERACCIÓN VIKINGO.</font>\n" +
                 "<font color=\"#1C1C1C\" size=\"9\"> · CHATS DE GRUPOS & HALL OF FAME.</font>\n", NotificationSettings.NOTIFICATION_ABOUT_IMG, ""));
        }
    }
}
