﻿using Cloud.HabboHotel.Users;
using Cloud.Communication.Packets.Outgoing.Handshake;

namespace Cloud.HabboHotel.Rooms.Chat.Commands.User
{
    class FlagMeCommand : IChatCommand
    {
        public string PermissionRequired => "command_flagme";
        public string Parameters => "";
        public string Description => "Cambiar tu nombre de usuario.";

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            
            if (!this.CanChangeName(Session.GetHabbo()))
            {
                Session.SendWhisper("Lo sentimos, parece que actualmente no tienen la opción de cambiar su nombre de usuario!");
                return;
            }

            Session.GetHabbo().ChangingName = true;
            Session.SendNotification("Tenga en cuenta que si su nombre de usuario se considerará como no apropiado, se le prohibió sin lugar a dudas.\r\r También tenga en cuenta que el personal no va a cambiar su nombre de usuario de nuevo en caso de tener un problema con lo que haya elegido.\r\rCierre esta ventana y haga clic en sí mismo para comenzar a elegir un nuevo nombre de usuario!");
            Session.SendMessage(new UserObjectComposer(Session.GetHabbo()));
        }

        private bool CanChangeName(Habbo Habbo)
        {
            if (Habbo.Rank == 1 && Habbo.VIPRank == 0 && Habbo.LastNameChange == 0)
                return true;
            else if (Habbo.Rank == 1 && Habbo.VIPRank == 1 && (Habbo.LastNameChange == 0 || (CloudServer.GetUnixTimestamp() + 604800) > Habbo.LastNameChange))
                return true;
            else if (Habbo.Rank == 1 && Habbo.VIPRank == 2 && (Habbo.LastNameChange == 0 || (CloudServer.GetUnixTimestamp() + 86400) > Habbo.LastNameChange))
                return true;
            else if (Habbo.Rank == 1 && Habbo.VIPRank == 3)
                return true;
            else if (Habbo.GetPermissions().HasRight("mod_tool"))
                return true;

            return false;
        }
    }
}
