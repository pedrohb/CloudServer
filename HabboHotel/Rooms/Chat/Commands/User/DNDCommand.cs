﻿namespace Cloud.HabboHotel.Rooms.Chat.Commands.User
{
    class DNDCommand : IChatCommand
    {
        public string PermissionRequired => "command_dnd";
        public string Parameters => "";
        public string Description => "Activar/Desactivar Mensajes de Consola.";

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            
            Session.GetHabbo().AllowConsoleMessages = !Session.GetHabbo().AllowConsoleMessages;
            Session.SendWhisper("Tu " + (Session.GetHabbo().AllowConsoleMessages == true ? "ahora" : "no") + " puedes recibir mensajes de consola.");
        }
    }
}