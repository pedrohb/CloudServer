﻿using System;
using System.Text;

namespace Cloud.HabboHotel.Rooms.Chat.Commands.User
{
    class StatsCommand : IChatCommand
    {
        public string PermissionRequired => "command_stats";
        public string Parameters => "";
        public string Description => "Ver las estadísticas actuales.";

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            double Minutes = Session.GetHabbo().GetStats().OnlineTime / 60;
            double Hours = Minutes / 60;
            int OnlineTime = Convert.ToInt32(Hours);
            string s = OnlineTime == 1 ? "" : "s";

            StringBuilder HabboInfo = new StringBuilder();
            HabboInfo.Append("Las estadísticas de su cuenta son:\r\r");

            HabboInfo.Append("Información de la moneda:\r");
            HabboInfo.Append("Créditos: " + Session.GetHabbo().Credits + "\r");
            HabboInfo.Append("Duckets: " + Session.GetHabbo().Duckets + "\r");
            HabboInfo.Append("Diamantes: " + Session.GetHabbo().Diamonds + "\r");
            HabboInfo.Append("Tiempo en línea: " + OnlineTime + " Hour" + s + "\r");
            HabboInfo.Append("Saludos: " + Session.GetHabbo().GetStats().Respect + "\r");
            HabboInfo.Append("Puntos GOTW: " + Session.GetHabbo().GOTWPoints + "\r\r");


            Session.SendNotification(HabboInfo.ToString());
        }
    }
}
