﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;
using Cloud.Communication.Packets.Outgoing.Users;
using Cloud.Communication.Packets.Outgoing.Notifications;
using Cloud.Communication.Packets.Outgoing.Handshake;
using Cloud.Communication.Packets.Outgoing.Quests;
using Cloud.HabboHotel.Items;
using Cloud.Communication.Packets.Outgoing.Inventory.Furni;
using Cloud.Communication.Packets.Outgoing.Catalog;
using Cloud.HabboHotel.Quests;
using Cloud.HabboHotel.Rooms;
using System.Threading;
using Cloud.HabboHotel.GameClients;
using Cloud.Communication.Packets.Outgoing.Rooms.Avatar;
using Cloud.Communication.Packets.Outgoing.Pets;
using Cloud.Communication.Packets.Outgoing.Messenger;
using Cloud.HabboHotel.Users.Messenger;
using Cloud.Communication.Packets.Outgoing.Rooms.Polls;
using Cloud.Communication.Packets.Outgoing.Rooms.Notifications;
using Cloud.Communication.Packets.Outgoing.Availability;
using Cloud.Communication.Packets.Outgoing;


namespace Cloud.HabboHotel.Rooms.Chat.Commands.Events
{
   class HelpCommand : IChatCommand
    {
        public string PermissionRequired => "command_info";
        public string Parameters => "[MENSAJE]";
        public string Description => "Envía una petición de ayuda, describiendo brevemente tu problema.";

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            long nowTime = CloudServer.CurrentTimeMillis();
            long timeBetween = nowTime - Session.GetHabbo()._lastTimeUsedHelpCommand;
            if (timeBetween < 60000)
            {
                Session.SendMessage(RoomNotificationComposer.SendBubble("abuse","Espera al menos 1 minuto para volver a usar el sistema de soporte.",""));
                return;
            }

            Session.GetHabbo()._lastTimeUsedHelpCommand = nowTime;
            string Request = CommandManager.MergeParams(Params, 1);

            if (Params.Length == 1)
            {
                Session.SendMessage(new RoomNotificationComposer("Sistema de soporte:", "<font color='#B40404'><b>¡Atención, " + Session.GetHabbo().Username + "!</b></font>\n\n<font size=\"11\" color=\"#1C1C1C\">El sistema de soporte ha sido creado para hacer peticiones de ayuda detalladas. Por lo que no puedes enviar un mensaje vacío ya que no tiene ninguna utilidad.\n\n" +
                 "Si quieres pedir ayuda, describe <font color='#B40404'> <b> detalladamente tu problema</b></font>. \n\nEl sistema detectará si abusas de estas peticiones, por lo que no envíes más de una o serás bloqueado.\n\n" +
                 "Recuerda que también dispones de la central de ayuda para resolver tus problemas.", "help_user", ""));
                return;
            }
            else

                CloudServer.GetGame().GetClientManager().GuideAlert(new RoomNotificationComposer("¡Nuevo caso de atención!",
                 "El usuario " + Session.GetHabbo().Username + " requiere la ayuda de un guía, embajador o moderador.<br></font></b><br>Su duda o problema es el siguiente:<br><b>s"
                 + Request + "</b></font><br><br>Atiende al usuario cuanto antes para resolver su duda, recuerda que dentro de poco tu ayuda será puntuada y eso se tendrá en cuenta para ascender.", "helpers", "Seguir a " + Session.GetHabbo().Username + "", "event:navigator/goto/" + Session.GetHabbo().CurrentRoomId));

            CloudServer.GetGame().GetAchievementManager().ProgressAchievement(Session, "ACH_GuideEnrollmentLifetime", 1);
            Session.SendMessage(RoomNotificationComposer.SendBubble("ambassador", "Tu petición de ayuda ha sido enviada correctamente, por favor espera.", ""));
        }
    }
}



