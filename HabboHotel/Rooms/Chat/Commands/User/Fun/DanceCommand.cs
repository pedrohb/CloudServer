﻿using Cloud.Communication.Packets.Outgoing.Rooms.Avatar;

namespace Cloud.HabboHotel.Rooms.Chat.Commands.User.Fun
{
    class DanceCommand :IChatCommand
    {
        public string PermissionRequired => "command_dance";
        public string Parameters => "[BAILE]";
        public string Description => "Demasiado perezoso para bailar la forma correcta? Hazlo asi!";

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            RoomUser ThisUser = Session.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (ThisUser == null)
                return;

            if (Params.Length == 1)
            {
                Session.LogsNotif("Por favor, introduzca un ID de un baile.", "command_notification");
                return;
            }

            int DanceId;
            if (int.TryParse(Params[1], out DanceId))
            {
                if (DanceId > 4 || DanceId < 0)
                {
                    Session.LogsNotif("El ID de la danza debe estar entre 0 y 4!", "command_notification");
                    return;
                }

                Session.GetHabbo().CurrentRoom.SendMessage(new DanceComposer(ThisUser, DanceId));
            }
            else
            {
                Session.LogsNotif("Por favor, introduzca un ID válido de baile.", "command_notification");
                return;
            }
        }
    }
}
