﻿using Cloud.Communication.Packets.Outgoing.Rooms.Notifications;
using Cloud.HabboHotel.Rooms.Games.Teams;

namespace Cloud.HabboHotel.Rooms.Chat.Commands.User.Fun
{
    class EnableCommand : IChatCommand
    {
        public string PermissionRequired => "command_enable";
        public string Parameters => "[EFFECTID]";
        public string Description => "Habilitar un efecto!";

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.SendWhisper("Usted debe escribir un ID Efecto");
                return;
            }

            if (!Room.EnablesEnabled && !Session.GetHabbo().GetPermissions().HasRight("mod_tool"))
            {
                Session.SendWhisper("Vaya, parece que el propietario de la sala ha deshabilitado la capacidad de utilizar el comando enable aquí");
                return;
            }

            RoomUser ThisUser = Session.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Username);
            if (ThisUser == null)
                return;

            if (ThisUser.RidingHorse)
            {
                Session.SendWhisper("No se puede activar un efecto mientras montas un caballo");
                return;
            }
            else if (ThisUser.Team != TEAM.NONE)
                return;
            else if (ThisUser.isLying)
                return;

            int EffectId = 0;
            if (!int.TryParse(Params[1], out EffectId))
                return;

            if (EffectId > int.MaxValue || EffectId < int.MinValue)
                return;

            if ((EffectId == 102 || EffectId == 187 || EffectId == 593 || EffectId == 596 || EffectId == 598) && !Session.GetHabbo().GetPermissions().HasRight("mod_tool"))
            {
                Session.SendWhisper("Lo sentimos, lamentablemente sólo los staff pueden activar este efecto.");
                return;
            }

            if ((EffectId == 592 || EffectId == 595 || EffectId == 597 && Session.GetHabbo()._guidelevel < 1))
            {
                Session.SendWhisper("Lo sentimos, no perteneces al equipo guía, es por ello que no puedes usar este efecto.");
                return;
            }

            if (EffectId == 594 && Session.GetHabbo()._croupier < 1)
            {
                Session.SendWhisper("Lo sentimos, este enable es solo para el equipo Croupier de Habbi");
                return;
            }

            if (EffectId == 599 && Session.GetHabbo()._builder < 1)
            {
                Session.SendWhisper("Lo sentimos, este enable es solo para el equipo BAW de Habbi");
                return;
            }

            if (EffectId == 44 && (Session.GetHabbo().Rank < 2))
            {
                Session.SendWhisper("Lo sentimos, no eres VIP, por ello no puedes usar dicho enable.");
                return;
            }

            if (EffectId == 178 && (!Session.GetHabbo().GetPermissions().HasRight("gold_vip") && !Session.GetHabbo().GetPermissions().HasRight("events_staff")))
            {
                Session.SendWhisper("Lo sentimos, este comando es sólo para los guías.");
                return;
            }

            Session.GetHabbo().Effects().ApplyEffect(EffectId);
        }
    }
}
