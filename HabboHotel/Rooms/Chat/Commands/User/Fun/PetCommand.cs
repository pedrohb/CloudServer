﻿using Cloud.Communication.Packets.Outgoing.Rooms.Engine;
using Cloud.Communication.Packets.Outgoing.Rooms.Notifications;
using System;
using System.Text;
using Cloud.Communication.Packets.Outgoing.Notifications;

namespace Cloud.HabboHotel.Rooms.Chat.Commands.User.Fun 
{
    class PetCommand : IChatCommand
    {
        public string PermissionRequired => "command_pet";
        public string Parameters => "";
        public string Description => "Permite transformar en un animal doméstico.";

        public void Execute(GameClients.GameClient Session, Room Room, string[] Params)
        {
            

            RoomUser RoomUser = Session.GetHabbo().CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (RoomUser == null)
                return;

            if (!Room.PetMorphsAllowed)
            {
                Session.SendWhisper("El propietario de la sala ha deshabilitado la capacidad de utilizar un morfo mascotas en esta habitación.");
                if (Session.GetHabbo().PetId > 0)
                {
                    Session.SendWhisper("Vaya usted, usted todavía tiene una metamorfosis, un-morphing.");
                    Session.GetHabbo().PetId = 0;

                    Room.SendMessage(new UserRemoveComposer(RoomUser.VirtualId));
                    Room.SendMessage(new UsersComposer(RoomUser));
                }
                return;
            }

            if (Params.Length == 1)
            {
                StringBuilder List = new StringBuilder("");
                Session.SendMessage(new MOTDNotificationComposer("Lista de Mascotas disponibles en "+CloudServer.HotelName+"\n" +
                "···········································\n" +
                ":pet habbo » Vuelve a la normalidad\n" +
                "····································\n" +
                ":pet dog » Transfórmate en perro\n" +
                "····································\n" +
                ":pet cat » Transfórmate en Gato\n" +
                "····································\n" +
                ":pet terrier » Transfórmate en esta adorable raza de perro \n" +
                "····································\n" +
                ":pet croc » ¿Quieres ser un cocodrilo? Usa este comando y atemoriza a tus amigos\n" +
                "····································\n" +
                ":pet bear » ¿Te gusta dar abrazos de oso? ¡Aquí tienes tu excusa perfecta!\n" +
                "····································\n" +
                ":pet pig » Transfórmate en Cerdo, pero no ensucies las salas, Frank no quiere trabajar mas de la cuenta...\n" +
                "····································\n" +
                ":pet lion » Transfórmate en un adorable Leon, pero no te comas a los usuarios...\n" +
                "····································\n" +
                ":pet rhino » Transfórmate en un rinoceronte, pero no juegues mucho con el cuerno...\n" +
                "····································\n" +
                ":pet spider » ¿Tienes algún amigo que tenga miedo a las arañas? ¡Gástale una broma transformándote en una!\n" +
                "····································\n" +
                ":pet turtle » Transfórmate en una tortuga, pero no seas tan lenta... \n" +
                "····································\n" +
                ":pet chick » El pollito pío puedes ser tu si usas este comando\n" +
                "····································\n" +
                ":pet frog » Salta sin parar con este comando\n" +
                "····································\n" +
                ":pet drag » Usa este comando para ser un temido dragón, pero no quemes las salas...\n" +
                "····································\n" +
                ":pet monkey » ¿Te gusta saltar por las ramas? ¡Utiliza este comando y sé un mono!\n" +
                "····································\n" +
                ":pet horse » Relincha sin parar!\n" +
                "····································\n" +
                ":pet bunny » Mucha gente ama a los conejos... ¿Porque no ser uno?\n" +
                "····································\n" +
                ":pet pigeon » ¡Conviértete en una paloma y vive libre! \n" +
                "····································\n" +
                ":pet demon » Satanás... ¿Eres tu?\n" +
                "····································\n" +
                ":pet gnome » Los gnomos son muy trabajadores... conviértete en uno!\n" +
                "····································\n" +
                ":pet raptor » ¡Volvamos a la prehistoria, pero no te comas a los usuarios!\n" +
                "····································\n" +
                ":pet pterodactyl » Vuela por las salas infundiendo terror\n" +
                "····································\n" +
                ":pet elefante » Cuidado, no vayas a pisar a nadie...\n" +
                "····································\n" +
                ":pet supermario » Conviertete en uno de los mas famosos personajes de videojuegos de la historia\n" +
                "····································\n" +
                ":pet lobo » Está permitido aullar...\n" +
                "····································\n" +
                ":pet pikachu » ¿Estás listo para electrocutar al Team Rocket?\n"));
                return;
            }

            int TargetPetId = GetPetIdByString(Params[1].ToString());
            if (TargetPetId == 0)
            {
                Session.SendWhisper("Vaya, no pudo encontrar una mascota con ese nombre!");
                return;
            }

            //Change the users Pet Id.
            Session.GetHabbo().PetId = (TargetPetId == -1 ? 0 : TargetPetId);

            //Quickly remove the old user instance.
            Room.SendMessage(new UserRemoveComposer(RoomUser.VirtualId));

            //Add the new one, they won't even notice a thing!!11 8-)
            Room.SendMessage(new UsersComposer(RoomUser));

            //Tell them a quick message.
            if (Session.GetHabbo().PetId > 0)
                Session.SendWhisper("Usa ':pet habbo' para regresarte a tu keko!");
        }

        private int GetPetIdByString(string Pet)
        {
            switch (Pet.ToLower())
            {
                default:
                    return 0;
                case "habbo":
                    return -1;
                case "perro":
                    return 60;//This should be 0.
                case "gato":
                case "1":
                    return 1;
                case "terrier":
                case "2":
                    return 2;
                case "croc":
                case "croco":
                case "3":
                    return 3;
                case "oso":
                case "4":
                    return 4;
                case "liz":
                case "cerdo":
                case "kill":
                case "5":
                    return 5;
                case "leon":
                case "rawr":
                case "6":
                    return 6;
                case "rhino":
                case "7":
                    return 7;
                case "spider":
                case "arana":
                case "araña":
                case "8":
                    return 8;
                case "tortuga":
                case "9":
                    return 9;
                case "chick":
                case "chicken":
                case "pollo":
                case "10":
                    return 10;
                case "frog":
                case "rana":
                case "11":
                    return 11;
                case "drag":
                case "dragon":
                case "12":
                    return 12;
                case "monkey":
                case "mono":
                case "14":
                    return 14;
                case "horse":
                case "caballo":
                case "15":
                    return 15;
                case "bunny":
                case "conejo":
                case "17":
                    return 17;
                case "pigeon":
                case "pajaro":
                case "21":
                    return 21;
                case "demon":
                case "demonio":
                case "23":
                    return 23;
                case "babybear":
                case "bebeoso":
                case "24":
                    return 24;
                case "babyterrier":
                case "bebeterrier":
                case "25":
                    return 25;
                case "gnome":
                case "gnomo":
                case "26":
                    return 26;
                case "kitten":
                case "gatito":
                case "28":
                    return 28;
                case "puppy":
                case "perrito":
                case "29":
                    return 29;
                case "piglet":
                case "cerdito":
                case "30":
                    return 30;
                case "haloompa":
                case "31":
                    return 31;
                case "rock":
                case "piedra":
                case "32":
                    return 32;
                case "pterodactyl":
                case "pterosaur":
                case "33":
                    return 33;
                case "raptor":
                case "velociraptor":
                case "34":
                    return 34;
                case "vaca":
                case "35":
                    return 35;
                case "pinguino":
                case "36":
                    return 36;
                case "elefante":
                case "37":
                    return 37;
                case "bebeguapo":
                case "38":
                    return 38;
                case "bebefeo":
                case "39":
                    return 39;
                case "supermario":
                case "mario":
                case "40":
                    return 40;
                case "pikachu":
                case "41":
                    return 41;
                case "lobo":
                case "42":
                    return 42;
            }
        }
    }
}