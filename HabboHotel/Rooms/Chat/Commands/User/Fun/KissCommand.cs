﻿using System;
using Cloud.HabboHotel.GameClients;
using Cloud.Communication.Packets.Outgoing.Rooms.Chat;
using Cloud.Communication.Packets.Outgoing.Rooms.Notifications;

namespace Cloud.HabboHotel.Rooms.Chat.Commands.User.Fun
{
    class KissCommand : IChatCommand
    {
        public string PermissionRequired => "command_kiss";
        public string Parameters => "[USUARIO]";
        public string Description => "Besa al Usuario <3";

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.SendWhisper("¡Por favor ingresa el nombre de un usuario!");
                return;
            }
            GameClient Target = CloudServer.GetGame().GetClientManager().GetClientByUsername(Params[1]);
            if (Target == null)
            {
                Session.SendWhisper("¡Lo sentimos, no hemos encontrado el usuario!");
                return;
            }
            else
            {
                RoomUser TargetID = Room.GetRoomUserManager().GetRoomUserByHabbo(Target.GetHabbo().Id);
                RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
                if (TargetID == null)
                {
                    Session.LogsNotif("¡Lo sentimos, el usuario no se encuentra en la sala!", "command_notification");
                    return;
                }
                else if (Target.GetHabbo().Username == Session.GetHabbo().Username)
                {
                    Session.SendWhisper("¿Estás loco? No te peudes besar a ti mismo.");
                    Room.SendMessage(new ChatComposer(User.VirtualId, "¡Alguien ayude a éste usuario, no tiene vida social!", 0, 34));
                    return;
                }
                else if (TargetID.TeleportEnabled)
                {
                    Session.LogsNotif("¡Lo sentimos, el usuario tiene activado el teleport!", "command_notification");
                    return;
                }
                else
                {
                    if (User != null)
                    {
                        if ((Math.Abs((int)(TargetID.X - User.X)) < 2) && (Math.Abs((int)(TargetID.Y - User.Y)) < 2))
                        {
                            Room.SendMessage(new ChatComposer(User.VirtualId, "*He besado a " + Params[1] + " en la boca*", 0, 16));
                            Room.SendMessage(new ChatComposer(TargetID.VirtualId, "*Se sonroja*", 0, 16));
                            TargetID.ApplyEffect(9);
                        }
                        else
                        {
                            Session.SendWhisper("¡Lo sentimos, el usuario no está cerca de tí!");
                            return;
                        }
                    }
                }
            }
        }

    }
}