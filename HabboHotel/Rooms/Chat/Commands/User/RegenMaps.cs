﻿namespace Cloud.HabboHotel.Rooms.Chat.Commands.User
{
    class RegenMaps : IChatCommand
    {
        public string PermissionRequired => "command_regen_maps";
        public string Parameters => "";
        public string Description => "Regenera el mapa de la sala!"; 

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            

            if (!Room.CheckRights(Session, true))
            {
                Session.SendWhisper("Vaya, sólo el propietario de esta sala se puede ejecutar este comando!");
                return;
            }

            Room.GetGameMap().GenerateMaps();
            Session.SendWhisper("mapa del juego de esta sala regenerado con éxito.");
        }
    }
}
