﻿namespace Cloud.HabboHotel.Rooms.Chat.Commands.User
{
    class SetSpeedCommand : IChatCommand
    {
        public string PermissionRequired => "command_setspeed";
        public string Parameters => "[VELOCIDAD]";
        public string Description => "Velocidad de los rollers.";

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            
            if (!Room.CheckRights(Session, true))
                return;

            if (Params.Length == 1)
            {
                Session.SendWhisper("Por favor, introduzca un valor para la velocidad del rodillo.");
                return;
            }

            int Speed;
            if (int.TryParse(Params[1], out Speed))
            {
                Session.GetHabbo().CurrentRoom.GetRoomItemHandler().SetSpeed(Speed);
            }
            else
                Session.SendWhisper("Cantidad no válida, por favor, introduzca un número válido.");
        }
    }
}