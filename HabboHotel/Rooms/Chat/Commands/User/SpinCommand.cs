﻿namespace Cloud.HabboHotel.Rooms.Chat.Commands.User.Fun
{
    class SpinCommand : IChatCommand
    {
        public string PermissionRequired => "command_spin";
        public string Parameters => "";
        public string Description => "Giro en un círculo"; 

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (User == null)
                return;
            
            User.ApplyEffect(500);
            Session.SendWhisper("Déjame girar en ese gatito ;)");
        }
    }
}