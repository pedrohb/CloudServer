﻿using Cloud.HabboHotel.GameClients;
using Cloud.Communication.Packets.Outgoing.Rooms.Notifications;
using Cloud.Database.Interfaces;
using System;

namespace Cloud.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class GiveRanksCommand : IChatCommand
    {
        public string PermissionRequired => "command_rank";
        public string Parameters => "[USUARIO] [TIPO] [RANGO]";
        public string Description => "Escribe :rank para ver la explicación.";

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.SendMessage(new MassEventComposer("habbopages/chat/giverankinfo.txt"));
                return;
            }

            GameClient Target = CloudServer.GetGame().GetClientManager().GetClientByUsername(Params[1]);
            if (Target == null)
            {
                Session.SendWhisper("Oops, no se ha conseguido este usuario.");
                return;
            }
            

            string RankType = Params[2];
            switch (RankType.ToLower())
            {
                case "encargado":
                    {
                        int NewRank = 15;
                        if(Session.GetHabbo().Rank < 12)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Session.GetHabbo().Rank < 16)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Target.GetHabbo().Rank == NewRank)
                        {
                            Session.SendWhisper("Oops, El usuario ya tiene este rango!");
                            break;
                        }
                        if (Target.GetHabbo().Rank >= Session.GetHabbo().Rank)
                        {
                            Session.SendWhisper("Oops, El usuario tiene un rango mayor que usted y no lo puede modificar!");
                            break;
                        }
                        //Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de Fundador. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                        Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                        Target.GetHabbo().Rank = NewRank;
                        using (IQueryAdapter dbClient = CloudServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.runFastQuery("UPDATE `users` SET `rank` = '" + NewRank + "' WHERE `id` = '" + Target.GetHabbo().Id + "' LIMIT 1");
                        }
                        Target.Disconnect();
                        break;
                    }

                case "manager":
                    {
                        int NewRank = 14;
                        if (Session.GetHabbo().Rank < 12)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Session.GetHabbo().Rank < 15)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Target.GetHabbo().Rank == NewRank)
                        {
                            Session.SendWhisper("Oops, El usuario ya tiene este rango!");
                            break;
                        }
                        if (Target.GetHabbo().Rank >= Session.GetHabbo().Rank)
                        {
                            Session.SendWhisper("Oops, El usuario tiene un rango mayor que usted y no lo puede modificar!");
                            break;
                        }
                        //Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de Fundador. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                        Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                        Target.GetHabbo().Rank = NewRank;
                        using (IQueryAdapter dbClient = CloudServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.runFastQuery("UPDATE `users` SET `rank` = '" + NewRank + "' WHERE `id` = '" + Target.GetHabbo().Id + "' LIMIT 1");
                        }
                        Target.Disconnect();
                        break;
                    }
                case "administrador":
                    {
                        int NewRank = 13;
                        if (Session.GetHabbo().Rank < 12)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Session.GetHabbo().Rank < 15)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Target.GetHabbo().Rank == NewRank)
                        {
                            Session.SendWhisper("Oops, El usuario ya tiene este rango!");
                            break;
                        }
                        if (Target.GetHabbo().Rank >= Session.GetHabbo().Rank)
                        {
                            Session.SendWhisper("Oops, El usuario tiene un rango mayor que usted y no lo puede modificar!");
                            break;
                        }
                        //Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de Fundador. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                        Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                        Target.GetHabbo().Rank = NewRank;
                        using (IQueryAdapter dbClient = CloudServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.runFastQuery("UPDATE `users` SET `rank` = '" + NewRank + "' WHERE `id` = '" + Target.GetHabbo().Id + "' LIMIT 1");
                        }
                        Target.Disconnect();
                        break;
                    }
                case "coadministrador":
                    {
                        int NewRank = 12;
                        if (Session.GetHabbo().Rank < 12)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Session.GetHabbo().Rank < 15)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Target.GetHabbo().Rank == NewRank)
                        {
                            Session.SendWhisper("Oops, El usuario ya tiene este rango!");
                            break;
                        }
                        if (Target.GetHabbo().Rank >= Session.GetHabbo().Rank)
                        {
                            Session.SendWhisper("Oops, El usuario tiene un rango mayor que usted y no lo puede modificar!");
                            break;
                        }
                        //Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de Fundador. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                        Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                        Target.GetHabbo().Rank = NewRank;
                        using (IQueryAdapter dbClient = CloudServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.runFastQuery("UPDATE `users` SET `rank` = '" + NewRank + "' WHERE `id` = '" + Target.GetHabbo().Id + "' LIMIT 1");
                        }
                        Target.Disconnect();
                        break;
                    }

                case "moderador":
                    {
                        int NewRank = 11;
                        if (Session.GetHabbo().Rank < 12)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Session.GetHabbo().Rank < 13)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Target.GetHabbo().Rank == NewRank)
                        {
                            Session.SendWhisper("Oops, El usuario ya tiene este rango!");
                            break;
                        }
                        if (Target.GetHabbo().Rank >= Session.GetHabbo().Rank)
                        {
                            Session.SendWhisper("Oops, El usuario tiene un rango mayor que usted y no lo puede modificar!");
                            break;
                        }
                        //Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de Fundador. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                        Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                        Target.GetHabbo().Rank = NewRank;
                        using (IQueryAdapter dbClient = CloudServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.runFastQuery("UPDATE `users` SET `rank` = '" + NewRank + "' WHERE `id` = '" + Target.GetHabbo().Id + "' LIMIT 1");
                        }
                        Target.Disconnect();
                        break;
                    }

                case "gamemaster":
                    {
                        int NewRank = 10;
                        if (Session.GetHabbo().Rank < 12)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Session.GetHabbo().Rank < 13)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Target.GetHabbo().Rank == NewRank)
                        {
                            Session.SendWhisper("Oops, El usuario ya tiene este rango!");
                            break;
                        }
                        if (Target.GetHabbo().Rank >= Session.GetHabbo().Rank)
                        {
                            Session.SendWhisper("Oops, El usuario tiene un rango mayor que usted y no lo puede modificar!");
                            break;
                        }
                        //Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de Fundador. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                        Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                        Target.GetHabbo().Rank = NewRank;
                        using (IQueryAdapter dbClient = CloudServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.runFastQuery("UPDATE `users` SET `rank` = '" + NewRank + "' WHERE `id` = '" + Target.GetHabbo().Id + "' LIMIT 1");
                        }
                        Target.Disconnect();
                        break;
                    }

                case "builder":
                    {
                        int NewRank = 9;
                        if (Session.GetHabbo().Rank < 11)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Session.GetHabbo().Rank < 11)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Target.GetHabbo().Rank == NewRank)
                        {
                            Session.SendWhisper("Oops, El usuario ya tiene este rango!");
                            break;
                        }
                        if (Target.GetHabbo().Rank >= Session.GetHabbo().Rank)
                        {
                            Session.SendWhisper("Oops, El usuario tiene un rango mayor que usted y no lo puede modificar!");
                            break;
                        }
                        //Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de Fundador. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                        Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                        Target.GetHabbo().Rank = NewRank;
                        using (IQueryAdapter dbClient = CloudServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.runFastQuery("UPDATE `users` SET `rank` = '" + NewRank + "' WHERE `id` = '" + Target.GetHabbo().Id + "' LIMIT 1");
                        }
                        Target.Disconnect();
                        break;
                    }

                case "colaborador":
                    {
                        int NewRank = 8;
                        if (Session.GetHabbo().Rank < 11)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Session.GetHabbo().Rank < 11)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Target.GetHabbo().Rank == NewRank)
                        {
                            Session.SendWhisper("Oops, El usuario ya tiene este rango!");
                            break;
                        }
                        if (Target.GetHabbo().Rank >= Session.GetHabbo().Rank)
                        {
                            Session.SendWhisper("Oops, El usuario tiene un rango mayor que usted y no lo puede modificar!");
                            break;
                        }
                        //Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de Fundador. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                        Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                        Target.GetHabbo().Rank = NewRank;
                        using (IQueryAdapter dbClient = CloudServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.runFastQuery("UPDATE `users` SET `rank` = '" + NewRank + "' WHERE `id` = '" + Target.GetHabbo().Id + "' LIMIT 1");
                        }
                        Target.Disconnect();
                        break;
                    }

                case "gamecreator":
                    {
                        int NewRank = 7;
                        if (Session.GetHabbo().Rank < 11)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Session.GetHabbo().Rank < 11)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Target.GetHabbo().Rank == NewRank)
                        {
                            Session.SendWhisper("Oops, El usuario ya tiene este rango!");
                            break;
                        }
                        if (Target.GetHabbo().Rank >= Session.GetHabbo().Rank)
                        {
                            Session.SendWhisper("Oops, El usuario tiene un rango mayor que usted y no lo puede modificar!");
                            break;
                        }
                        //Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de Fundador. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                        Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                        Target.GetHabbo().Rank = NewRank;
                        using (IQueryAdapter dbClient = CloudServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.runFastQuery("UPDATE `users` SET `rank` = '" + NewRank + "' WHERE `id` = '" + Target.GetHabbo().Id + "' LIMIT 1");
                        }
                        Target.Disconnect();
                        break;
                    }

                case "inter":
                    {
                        int NewRank = 6;
                        if (Session.GetHabbo().Rank < 8)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Session.GetHabbo().Rank < 8)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Target.GetHabbo().Rank == NewRank)
                        {
                            Session.SendWhisper("Oops, El usuario ya tiene este rango!");
                            break;
                        }
                        if (Target.GetHabbo().Rank >= Session.GetHabbo().Rank)
                        {
                            Session.SendWhisper("Oops, El usuario tiene un rango mayor que usted y no lo puede modificar!");
                            break;
                        }
                        //Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de Fundador. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                        Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                        Target.GetHabbo().Rank = NewRank;
                        using (IQueryAdapter dbClient = CloudServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.runFastQuery("UPDATE `users` SET `rank` = '" + NewRank + "' WHERE `id` = '" + Target.GetHabbo().Id + "' LIMIT 1");
                        }
                        Target.Disconnect();
                        break;
                    }


                case "ayuda":
                    {
                        int NewRank = 5;
                        if (Session.GetHabbo().Rank < 8)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Session.GetHabbo().Rank < 8)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Target.GetHabbo().Rank == NewRank)
                        {
                            Session.SendWhisper("Oops, El usuario ya tiene este rango!");
                            break;
                        }
                        if (Target.GetHabbo().Rank >= Session.GetHabbo().Rank)
                        {
                            Session.SendWhisper("Oops, El usuario tiene un rango mayor que usted y no lo puede modificar!");
                            break;
                        }
                        //Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de Fundador. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                        Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                        Target.GetHabbo().Rank = NewRank;
                        using (IQueryAdapter dbClient = CloudServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.runFastQuery("UPDATE `users` SET `rank` = '" + NewRank + "' WHERE `id` = '" + Target.GetHabbo().Id + "' LIMIT 1");
                        }
                        Target.Disconnect();
                        break;
                    }

                case "soporte":
                    {
                        int NewRank = 4;
                        if (Session.GetHabbo().Rank < 8)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Session.GetHabbo().Rank < 8)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Target.GetHabbo().Rank == NewRank)
                        {
                            Session.SendWhisper("Oops, El usuario ya tiene este rango!");
                            break;
                        }
                        if (Target.GetHabbo().Rank >= Session.GetHabbo().Rank)
                        {
                            Session.SendWhisper("Oops, El usuario tiene un rango mayor que usted y no lo puede modificar!");
                            break;
                        }
                        //Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de Fundador. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                        Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                        Target.GetHabbo().Rank = NewRank;
                        using (IQueryAdapter dbClient = CloudServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.runFastQuery("UPDATE `users` SET `rank` = '" + NewRank + "' WHERE `id` = '" + Target.GetHabbo().Id + "' LIMIT 1");
                        }
                        Target.Disconnect();
                        break;
                    }

                case "publicista":
                    {
                        int NewRank = 3;
                        if (Session.GetHabbo().Rank < 8)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Session.GetHabbo().Rank < 8)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Target.GetHabbo().Rank == NewRank)
                        {
                            Session.SendWhisper("Oops, El usuario ya tiene este rango!");
                            break;
                        }
                        if (Target.GetHabbo().Rank >= Session.GetHabbo().Rank)
                        {
                            Session.SendWhisper("Oops, El usuario tiene un rango mayor que usted y no lo puede modificar!");
                            break;
                        }
                        //Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de Fundador. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                        Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                        Target.GetHabbo().Rank = NewRank;
                        using (IQueryAdapter dbClient = CloudServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.runFastQuery("UPDATE `users` SET `rank` = '" + NewRank + "' WHERE `id` = '" + Target.GetHabbo().Id + "' LIMIT 1");
                        }
                        Target.Disconnect();
                        break;
                    }

                case "linces":
                    {
                        int NewRank = 2;
                        if (Session.GetHabbo().Rank < 5)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Session.GetHabbo().Rank < 5)
                        {
                            Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                            break;
                        }
                        if (Target.GetHabbo().Rank == NewRank)
                        {
                            Session.SendWhisper("Oops, El usuario ya tiene este rango!");
                            break;
                        }
                        if (Target.GetHabbo().Rank >= Session.GetHabbo().Rank)
                        {
                            Session.SendWhisper("Oops, El usuario tiene un rango mayor que usted y no lo puede modificar!");
                            break;
                        }
                        //Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de Fundador. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                        Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                        Target.GetHabbo().Rank = NewRank;
                        using (IQueryAdapter dbClient = CloudServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.runFastQuery("UPDATE `users` SET `rank` = '" + NewRank + "' WHERE `id` = '" + Target.GetHabbo().Id + "' LIMIT 1");
                        }
                        Target.Disconnect();
                        break;
                    }


                default:
                    Session.SendWhisper(RankType + "' no es un rango disponible para otorgar.");
                    break;
            }



                #region Viejo Metodo
                /*
                string RankType = Params[2];
                switch (RankType.ToLower())
                {
                    case "guia":
                    case "guide":
                        {
                            if (Session.GetHabbo().Rank < 6)
                            {
                                Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                                break;
                            }
                            else
                            {
                                int Rank;
                                if (int.TryParse(Params[3], out Rank))
                                {
                                    byte RankByte = Convert.ToByte(Rank);
                                    Target.GetHabbo()._guidelevel = RankByte;

                                    switch (RankByte)
                                    {
                                        case 0:
                                            Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de retirarte del departamento de soporte. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                                            Session.SendWhisper("Rango retirado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                                            Target.Disconnect();
                                            break;

                                        case 1:
                                            Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de guía conejo. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                                            Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                                            break;

                                        case 2:
                                            Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de guía búho. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                                            Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                                            break;

                                        case 3:
                                            Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de encargado de guías. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                                            Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                                            break;

                                        case 4:
                                            Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de encargado de guías oculto. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                                            Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                                            break;
                                    }

                                    break;
                                }

                                else { Session.SendWhisper("Oops, " + RankType + " no es un valor válido para otorgar."); break; }
                            }
                        }

                    case "publi":
                    case "publicista":
                        {
                            if (Session.GetHabbo().Rank < 6)
                            {
                                Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                                break;
                            }
                            else
                            {
                                int Rank;
                                if (int.TryParse(Params[3], out Rank))
                                {
                                    byte RankByte = Convert.ToByte(Rank);
                                    Target.GetHabbo()._publicistalevel = RankByte;

                                    switch (RankByte)
                                    {
                                       case 0:
                                       Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de retirarte del departamento de publicistas. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                                       Session.SendWhisper("Rango retirado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                                       Target.Disconnect();
                                       break;

                                       case 1:
                                       Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de publicista a prueba. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                                       Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                                       break;

                                       case 2:
                                       Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de publicista. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                                       Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                                       //Target.GetHabbo().GetBadgeComponent().GiveBadge("BADGE", true, Target);
                                       break;

                                       case 3:
                                       Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de encargado de publicidad. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));                                        Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                                       break;

                                       case 4:
                                       Target.SendMessage(RoomNotificationComposer.SendBubble("eventoxx", Session.GetHabbo().Username + " acaba de darte el rango de encargado de publicidad oculto. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                                       Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                                       break;
                                    }
                                    break;
                                }
                                else
                                {
                                    Session.SendWhisper("Oops, " + Rank + " no es un valor válido para otorgar.");
                                    break;
                                }
                            }
                        }

                    case "inter":
                    case "croupier":
                        {
                            if (Session.GetHabbo().Rank < 6)
                            {
                                Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                                break;
                            }
                            else
                            {
                                int Rank;
                                if (int.TryParse(Params[3], out Rank))
                                {
                                    byte RankByte = Convert.ToByte(Rank);
                                    Target.GetHabbo()._publicistalevel = RankByte;

                                    switch (RankByte)
                                    {
                                        case 0:
                                        Target.SendMessage(RoomNotificationComposer.SendBubble("inters", Session.GetHabbo().Username + " acaba de retirarte del departamento de intermediarios. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                                        Session.SendWhisper("Rango retirado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                                        Target.GetHabbo().GetBadgeComponent().RemoveBadge("INT3");
                                        Target.Disconnect();
                                        break;

                                        case 1:
                                        Target.SendMessage(RoomNotificationComposer.SendBubble("inters", Session.GetHabbo().Username + " acaba de darte el cargo de intermediario. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                                        Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                                        Target.GetHabbo().GetBadgeComponent().GiveBadge("INT3", true, Target);
                                        break;
                                    }
                                    break;
                                }
                                else
                                {
                                    Session.SendWhisper("Oops, " + Rank + " no es un valor válido para otorgar.");
                                    break;
                                }
                            }
                        }

                    case "builder":
                        {
                            if (Session.GetHabbo().Rank < 6)
                            {
                                Session.SendWhisper("Oops, usted no tiene los permisos necesarios para usar este comando!");
                                break;
                            }
                            else
                            {
                                int Rank;
                                if (int.TryParse(Params[3], out Rank))
                                {
                                    byte RankByte = Convert.ToByte(Rank);
                                    Target.GetHabbo()._publicistalevel = RankByte;

                                    switch (RankByte)
                                    {
                                        case 0:
                                            Target.SendMessage(RoomNotificationComposer.SendBubble("builder", Session.GetHabbo().Username + " acaba de retirarte del departamento de BAW. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                                            Session.SendWhisper("Rango retirado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                                            Target.GetHabbo().GetBadgeComponent().RemoveBadge("BU1LD");
                                            Target.Disconnect();
                                            break;

                                        case 1:
                                            Target.SendMessage(RoomNotificationComposer.SendBubble("builder", Session.GetHabbo().Username + " acaba de darte el cargo de BAW. Reinicia para aplicar los cambios respectivos.\n\nRecuerda que hemos depositado nuestra confianza en tí y que todo esfuerzo tiene su recompensa.", ""));
                                            Session.SendWhisper("Rango entregado satisfactoriamente a " + Target.GetHabbo().Username + ".");
                                            Target.GetHabbo().GetBadgeComponent().GiveBadge("BU1LD", true, Target);
                                            break;
                                    }
                                    break;
                                }
                                else
                                {
                                    Session.SendWhisper("Oops, " + Rank + " no es un valor válido para otorgar.");
                                    break;
                                }
                            }
                        }

                    default:
                        Session.SendWhisper(RankType + "' no es un rango disponible para otorgar.");
                        break;
                }*/

                #endregion

            }
    }
}