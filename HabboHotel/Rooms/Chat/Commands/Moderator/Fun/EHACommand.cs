﻿using Cloud.Communication.Packets.Outgoing.Rooms.Notifications;
using Cloud.HabboHotel.GameClients;

namespace Cloud.HabboHotel.Rooms.Chat.Commands.Events
{
    class EHACommand : IChatCommand
    {
        public string PermissionRequired => "command_event_alert";
        public string Parameters => "[MENSAJE]";
        public string Description => "Enviar una alerta de hotel para su evento!";

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            if (Session != null)
            {
                if (Room != null)
                {
                    string Message = "" +  "Hey, hay un evento sucediendo ahora mismo, ve de que se trata!";
                    if (Params.Length > 2)
                        Message = CommandManager.MergeParams(Params, 1);

                    CloudServer.GetGame().GetClientManager().SendMessage(new RoomNotificationComposer("Evento en marcha", Message + "\r\n- <b>" + Session.GetHabbo().Username + "</b>\r\n<i></i>", "figure/" + Session.GetHabbo().Username + "", "Go to \"" + Session.GetHabbo().CurrentRoom.Name + "\"!", "event:navigator/goto/" + Session.GetHabbo().CurrentRoomId));
                }
            }
        }
    }
}