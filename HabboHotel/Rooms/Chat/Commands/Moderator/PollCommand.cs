﻿using Cloud.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cloud.HabboHotel.Rooms.Chat.Commands.Moderator
{
    internal class PollCommand : IChatCommand
    {
        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            if (Params.Length == 0)
            {
                Session.SendWhisper("Por favor introduce la pregunta");
            }
            else
            {

                string quest = CommandManager.MergeParams(Params, 1);
                if (quest == "end")
                {
                    Room.endQuestion();
                }
                else
                {
                    Room.startQuestion(quest);
                }

            }
        }

        public string Description =>
            "Realizar una encuesta rápida.";

        public string Parameters =>
            "%question%";

        public string PermissionRequired =>
            "command_give_badge";
    }
}