﻿using Cloud.Database.Interfaces;
using Cloud.Communication.Packets.Outgoing.Rooms.Notifications;

namespace Cloud.HabboHotel.Rooms.Chat.Commands.Administrator
{
    class BubbleBotCommand : IChatCommand
    {
        public string PermissionRequired => "command_bubble";
        public string Parameters => "[BOTNAME] [BUBBLEID]";
        public string Description => "Utilice una burbuja a medida para charlar.";

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (User == null)
                return;

            if (Params.Length == 1)
            {
                Session.SendWhisper("Uy, no olvide introducir el nombre del bot!");
                return;
            }

            if (Params.Length == 2)
            {
                Session.SendWhisper("Uy, se olvidó introducir una ID de la burbuja!");
                return;
            }
            string BotName = CommandManager.MergeParams(Params, 1);
            string Bubble = CommandManager.MergeParams(Params, 2);
            using (IQueryAdapter dbClient = CloudServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.runFastQuery("UPDATE `bots` SET `chat_bubble` =  '" + Params[2] + "' WHERE `name` =  '" + Params[1] + "' AND  `room_id` =  '" + Session.GetHabbo().CurrentRoomId + "'");
                Session.LogsNotif("Le has cambiado la burbuja del chat al bot: " + Params[1] + "!", "command_notification");
            }
        }
    }
}