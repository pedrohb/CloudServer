﻿using Cloud.Communication.Packets.Outgoing.Rooms.Notifications;
using Cloud.HabboHotel.Rooms.Chat.Styles;
using Cloud.Database.Interfaces;

namespace Cloud.HabboHotel.Rooms.Chat.Commands.Administrator
{
    class BubbleCommand : IChatCommand
    {
        public string PermissionRequired => "command_bubble";
        public string Parameters => "[BUBBLEID]";
        public string Description => "Utilice una burbuja a medida para charlar.";

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (User == null)
                return;

            if (Params.Length == 1)
            {
                Session.SendWhisper("Uy, se olvidó introducir una ID de la burbuja!");
                return;
            }

            int Bubble = 0;
            if (!int.TryParse(Params[1].ToString(), out Bubble))
            {
                Session.SendWhisper("Por favor ingrese un número valido.");
                return;
            }

            if ((Bubble == 33) && !Session.GetHabbo().GetPermissions().HasRight("mod_tool"))
            {
                Session.LogsNotif("Lo sentimos, sólo los miembros del personal pueden utilizar estas bubbles.", "command_notification");
                return;
            }

            ChatStyle Style = null;
            if (!CloudServer.GetGame().GetChatManager().GetChatStyles().TryGetStyle(Bubble, out Style) || (Style.RequiredRight.Length > 0 && !Session.GetHabbo().GetPermissions().HasRight(Style.RequiredRight)))
            {
                Session.SendWhisper("Vaya, no se puede utilizar esta burbuja debido a un requisito de rango, lo siento!");
                return;
            }

            User.LastBubble = Bubble;
            Session.GetHabbo().CustomBubbleId = Bubble;
            Session.SendWhisper("Burbuja se establecida a: " + Bubble);
            using (IQueryAdapter dbClient = CloudServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.runFastQuery("UPDATE `users` SET `bubble_id` = '" + Bubble + "' WHERE `id` = '" + Session.GetHabbo().Id + "' LIMIT 1");
            }
        }
    }
}