﻿using System;

namespace Cloud.HabboHotel.Rooms.Chat.Commands.Administrator
{
    class CarryCommand : IChatCommand
    {
        public string PermissionRequired => "command_carry";
        public string Parameters => "[ITEMID]";
        public string Description => "Le permite llevar un elemento de la mano.";

        public void Execute(GameClients.GameClient Session, Room Room, string[] Params)
        {
            
            int ItemId = 0;
            if (!int.TryParse(Convert.ToString(Params[1]), out ItemId))
            {
                Session.SendWhisper("Por favor, introduzca un número entero válido.");
                return;
            }

            RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (User == null)
                return;

            User.CarryItem(ItemId);
        }
    }
}
