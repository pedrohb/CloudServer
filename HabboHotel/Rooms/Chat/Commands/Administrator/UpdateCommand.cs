﻿using System.Linq;
using Cloud.Communication.Packets.Outgoing.Rooms.Notifications;
using Cloud.Communication.Packets.Outgoing.Catalog;
using Cloud.Core;
using Cloud.HabboHotel.GameClients;
using Cloud.Communication.Packets.Outgoing.Notifications;
using System.Text;
using Cloud.Communication.Packets.Incoming.LandingView;
using Cloud.HabboHotel.LandingView;
using Cloud.HabboHotel.Rooms.TraxMachine;

namespace Cloud.HabboHotel.Rooms.Chat.Commands.Administrator
{
    class UpdateCommand : IChatCommand
    {
        public string PermissionRequired => "command_update"; 
        public string Parameters => "[VARIABLE]";
        public string Description => "Actualizar una parte específica del hotel.";

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                StringBuilder List = new StringBuilder();
                List.Append("- LISTA DE COMANDOS STAFF -\n\n");
                List.Append(":update catalogue - Actualiza el catalogo.\n········································································\n");
                List.Append(":update items - Actualiza los items.\n········································································\n");
                List.Append(":update jukebox - Actualiza las canciones agregadas.\n········································································\n");
                List.Append(":update wordfilter - Actualiza el filtro del hotel.\n········································································\n");
                List.Append(":update models - Actualiza el filtro del hotel.\n········································································\n");
                List.Append(":update promotions - Actualiza las promociones.\n········································································\n");
                List.Append(":update halloffame - Actualiza el salón de la fama.\n········································································\n");
                List.Append(":update youtube - Actualiza los videos de las TV's.\n········································································\n");
                List.Append(":update permissions - Actualiza los permisos de cada rango.\n········································································\n");
                List.Append(":update settings - Actualiza las configuraciones del hotel.\n········································································\n");
                List.Append(":update bans - Actualiza los baneos del hotel.\n········································································\n");
                List.Append(":update quests - Actualiza las Quests del hotel.\n········································································\n");
                List.Append(":update achievements - Actualiza los logros de los usuarios.\n········································································\n");
                List.Append(":update bots - Actualiza los bots del hotel.\n········································································\n");
                List.Append(":update achievements - Actualiza los logros de los usuarios.\n········································································\n");
                Session.SendMessage(new MOTDNotificationComposer(List.ToString()));
                return;
            }

            string UpdateVariable = Params[1];
            switch (UpdateVariable.ToLower())
            {
                case "cata":
                case "catalog":
                case "catalogue":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_catalog"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el 'command_update_catalog' permiso", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetCatalog().Init(CloudServer.GetGame().GetItemManager());
                        CloudServer.GetGame().GetClientManager().SendMessage(new CatalogUpdatedComposer());
                        Session.LogsNotif("Catalogo actualizado correctamente", "catalogue");
                        break;

                case "discos":
                case "songs":
                case "jukebox":
                case "canciones":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_songsdata"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el 'command_songsdata", "advice");
                            break;
                        }
                    int count = TraxSoundManager.Songs.Count;
                    TraxSoundManager.Init();
                    Session.LogsNotif("Músicas recargadas con exito, diferencia de longitud: " + checked(count - TraxSoundManager.Songs.Count), "advice");
                    break;

                case "wordfilter":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_filter"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el 'command_update_filter' permiso", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetChatManager().GetFilter().InitWords();
                        CloudServer.GetGame().GetChatManager().GetFilter().InitCharacters();
                        Session.LogsNotif("Filtro actualizado correctamente", "advice");
                        break;

                case "items":
                case "furni":
                case "furniture":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_furni"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el 'command_update_furni' permiso", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetItemManager().Init();
                        Session.LogsNotif("Items actualizados correctamente", "advice");
                        break;

                case "models":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_models"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el 'command_update_models' permiso", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetRoomManager().LoadModels();
                        Session.LogsNotif("Salas actualizadas correctamente.", "advice");
                        break;

                case "promotions":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_promotions"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el 'command_update_promotions' permiso.", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetLandingManager().LoadPromotions();
                        Session.LogsNotif("Promociones actualizadas correctamente.", "advice");
                        break;

                case "halloffame":
                case "salondelafama":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_halloffame"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el 'command_update_halloffame' permiso.", "advice");
                            break;
                        }

                        GetHallOfFame.getInstance().Load();
                        Session.LogsNotif("Hall of Fame actualizado con exito.", "advice");
                        break;

                case "youtube":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_youtube"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el 'command_update_youtube' permiso.", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetTelevisionManager().Init();
                        Session.LogsNotif("TV's actualizados.", "advice");
                        break;

                case "navigator":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_navigator"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el permiso 'command_update_navigator'.", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetNavigator().Init();
                        Session.LogsNotif("Navegador de salas actualizado.", "advice");
                        break;

                case "ranks":
                case "rights":
                case "permissions":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_rights"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el permiso 'command_update_rights'.", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetPermissionManager().Init();

                        foreach (GameClient Client in CloudServer.GetGame().GetClientManager().GetClients.ToList())
                        {
                            if (Client == null || Client.GetHabbo() == null || Client.GetHabbo().GetPermissions() == null)
                                continue;

                            Client.GetHabbo().GetPermissions().Init(Client.GetHabbo());
                        }

                        Session.LogsNotif("Permisos actualizados.", "advice");
                        break;
                case "pinatas":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_catalog"))
                        {
                            Session.SendWhisper("Oops, usted no tiene permiso para actualizar los premios de las piñatas.");
                            break;
                        }

                        CloudServer.GetGame().GetPinataManager().Initialize(CloudServer.GetDatabaseManager().GetQueryReactor());
                        CloudServer.GetGame().GetClientManager().SendMessage(RoomNotificationComposer.SendBubble("catalogue", "Premios Actualizados", ""));
                        break;
                case "crafting":
                    if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_furni"))
                    {
                        Session.SendWhisper("Oops, usted no tiene permiso para actualizar el crafting.");
                        break;
                    }

                    CloudServer.GetGame().GetCraftingManager().Init();
                    Session.SendWhisper("Crafting actualizado correctamente.");
                    break;
                case "crackable":
                case "ecotron":
                case "pinata":
                case "piñata":
                    CloudServer.GetGame().GetPinataManager().Initialize(CloudServer.GetDatabaseManager().GetQueryReactor());
                    CloudServer.GetGame().GetFurniMaticRewardsMnager().Initialize(CloudServer.GetDatabaseManager().GetQueryReactor());
                    CloudServer.GetGame().GetTargetedOffersManager().Initialize(CloudServer.GetDatabaseManager().GetQueryReactor());
                    break;

                case "relampago":
                case "targeted":
                case "targetedoffers":
                    CloudServer.GetGame().GetTargetedOffersManager().Initialize(CloudServer.GetDatabaseManager().GetQueryReactor());
                    break;

                case "config":
                case "settings":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_configuration"))
                        {
                            Session.LogsNotif("Ups, usted no tiene e permiso 'command_update_configuration'.", "advice"); ;
                            break;
                        }

                        CloudServer.GetGame().GetSettingsManager().Init();
                        ExtraSettings.RunExtraSettings();
                        CatalogSettings.RunCatalogSettings();
                        NotificationSettings.RunNotiSettings();
                        CloudServer.GetGame().GetTargetedOffersManager().Initialize(CloudServer.GetDatabaseManager().GetQueryReactor());
                        Session.LogsNotif("Configuraciones actualizadas.", "advice");
                        break;

                case "bans":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_bans"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el 'command_update_bans' permiso.", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetModerationManager().ReCacheBans();
                        Session.LogsNotif("Cache Ban re-cargado.", "advice");
                        break;

                case "quests":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_quests"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el 'command_update_quests' permiso.", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetQuestManager().Init();
                        Session.LogsNotif("Quests actualizadas.", "advice");
                        break;

                case "achievements":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_achievements"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el 'command_update_achievements' permiso.", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetAchievementManager().LoadAchievements();
                        Session.LogsNotif("Achievements actualizados.", "advice");
                        break;

                case "moderation":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_moderation"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el 'command_update_moderation' permiso.", "advice"); ;
                            break;
                        }

                        CloudServer.GetGame().GetModerationManager().Init();
                        CloudServer.GetGame().GetClientManager().ModAlert("Presets de moderación se han actualizado.Por favor, vuelva a cargar el cliente para ver los nuevos presets.");

                        Session.LogsNotif("Configuraciones del moderador actualizadas.", "advice");
                        break;

                case "vouchers":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_vouchers"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el permiso 'command_update_vouchers.", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetCatalog().GetVoucherManager().Init();
                        Session.LogsNotif("Caché de las Licencia del Catálogo Se Ha Actualizado Correctamente.", "advice");
                        break;

                case "gc":
                case "games":
                case "gamecenter":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_game_center"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el permiso 'command_update_game_center'.", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetGameDataManager().Init();
                        Session.LogsNotif("El Cache del Game Center Se Ha Actualizado Correctamente.", "advice");
                        break;

                case "pet_locale":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_pet_locale"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el permiso 'command_update_pet_locale'.", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetChatManager().GetPetLocale().Init();
                        CloudServer.GetGame().GetChatManager().GetPetCommands().Init();
                        Session.LogsNotif("Caché Local De Mascotas Actualizado Correctamente.", "advice");
                        break;

                case "locale":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_locale"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el permiso 'command_update_locale'.", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetLanguageManager().Init();
                        Session.LogsNotif("Locale caché actualizado correctamente.", "advice");
                        break;

                case "mutant":

                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_anti_mutant"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el permiso 'command_update_anti_mutant'.", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetFigureManager().Init();
                        Session.LogsNotif("FigureData manager Se Volvio A Cargar Con Éxito!", "advice");
                        break;

                case "bots":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_bots"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el permiso 'command_update_bots'.", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetBotManager().Init();
                        Session.LogsNotif("Bots actualizados.", "advice");
                        break;

                case "rewards":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_rewards"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el permiso 'command_update_rewards'.", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetRewardManager().Reload();
                        Session.LogsNotif("Gestor De Recompensas Se Volvio A Cargar Con Éxito!", "advice");
                        break;

                case "chat_styles":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_chat_styles"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el permiso 'command_update_chat_styles'.", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetChatManager().GetChatStyles().Init();
                        Session.LogsNotif("Estilos De Chat Se Volvio A Cargar Con Éxito!", "advice");
                        break;

				case "definitions":	
                case "badge_definitions":
                        if (!Session.GetHabbo().GetPermissions().HasCommand("command_update_badge_definitions"))
                        {
                            Session.LogsNotif("Ups, usted no tiene el permiso 'command_update_badge_definitions'.", "advice");
                            break;
                        }

                        CloudServer.GetGame().GetBadgeManager().Init();
                        Session.LogsNotif("Definiciones de Placas Se Volvio A Cargar Con Éxito!", "advice");
                        break;

                default:
                    Session.LogsNotif("'" + UpdateVariable + "' is not a valid thing to reload.", "advice");
                    break;
            }
        }
    }
}
