using Cloud.HabboHotel.Items;
using System;

namespace Cloud.HabboHotel.Catalog
{
	public class CatalogItemValue
	{
		public int Id
		{
			get;
			set;
		}

		public ItemData Base
		{
			get;
			set;
		}

		public int Amount
		{
			get;
			set;
		}

		public string ExtraData
		{
			get;
			set;
		}

		public int LimitedEditionStack
		{
			get;
			set;
		}

		public int LimitedEditionSells
		{
			get;
			set;
		}

		public CatalogItem Parent
		{
			get;
			set;
		}

		public bool IsLimited
		{
			get
			{
				return this.LimitedEditionStack > 0;
			}
		}

		public char Type
		{
			get
			{
				return this.Base.Type;
			}
		}

		public int SpriteId
		{
			get
			{
				return this.Base.SpriteId;
			}
		}

		public CatalogItemValue(CatalogItem Parent, int id, ItemData baseitem, int amount, string extradata, int limitedstack = 0, int limitedsells = 0)
		{
			this.Id = id;
			this.Base = baseitem;
			this.Amount = amount;
			this.ExtraData = extradata;
			this.LimitedEditionStack = limitedstack;
			this.LimitedEditionSells = limitedsells;
			this.Parent = Parent;
		}
	}
}
