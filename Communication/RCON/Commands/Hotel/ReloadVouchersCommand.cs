﻿namespace Cloud.Communication.RCON.Commands.Hotel
{
    class ReloadVouchersCommand : IRCONCommand
    {
        public string Description => "Se utiliza para recargar los vouchers";
        public string Parameters => "";

        public bool TryExecute(string[] parameters)
        {
            CloudServer.GetGame().GetCatalog().GetVoucherManager().Init();
            return true;
        }
    }
}