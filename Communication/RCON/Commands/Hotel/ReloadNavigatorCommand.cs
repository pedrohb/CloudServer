﻿namespace Cloud.Communication.RCON.Commands.Hotel
{
    class ReloadNavigatorCommand : IRCONCommand
    {
        public string Description => "Se utiliza para actualizar el navegador de salas";
        public string Parameters => "";

        public bool TryExecute(string[] parameters)
        {
            CloudServer.GetGame().GetNavigator().Init();

            return true;
        }
    }
}