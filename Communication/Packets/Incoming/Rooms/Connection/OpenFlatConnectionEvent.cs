﻿using System;

using Cloud.Communication.Packets.Incoming;
using Cloud.HabboHotel.GameClients;
using Cloud.Communication.Packets.Outgoing;
using Cloud.Communication.Packets.Outgoing.Rooms.Nux;

namespace Cloud.Communication.Packets.Incoming.Rooms.Connection
{
    public class OpenFlatConnectionEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            if (Session == null || Session.GetHabbo() == null)
                return;

            int RoomId = Packet.PopInt();
            string Password = Packet.PopString();
        
            Session.GetHabbo().PrepareRoom(RoomId, Password);
            
        }
    }
}