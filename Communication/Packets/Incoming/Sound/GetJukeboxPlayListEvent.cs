﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Cloud.Communication.Packets.Outgoing.Sound;
using Cloud.HabboHotel.GameClients;
using Cloud.Communication.Packets.Outgoing;

namespace Cloud.Communication.Packets.Incoming.Sound
{
    class GetJukeboxPlayListEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            if (Session.GetHabbo().CurrentRoom != null)
                Session.SendMessage(new SetJukeboxPlayListComposer(Session.GetHabbo().CurrentRoom));
        }
    }
}
