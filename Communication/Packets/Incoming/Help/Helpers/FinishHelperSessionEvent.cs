﻿using Cloud.Communication.Packets.Outgoing.Help.Helpers;
using Cloud.Communication.Packets.Outgoing.Rooms.Notifications;
using Cloud.HabboHotel.GameClients;
using Cloud.HabboHotel.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cloud.Communication.Packets.Incoming.Help.Helpers
{
    class FinishHelperSessionEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            var Voted = Packet.PopBoolean();
            var Element = HelperToolsManager.GetElement(Session);
            if (Element is HelperCase)
            {
                if (Voted)
                    Element.OtherElement.Session.SendMessage(RoomNotificationComposer.SendBubble("ambassador", "" + Session.GetHabbo().Username + ", gracias por colaborar en el programa de Alfas, has atendido satisfactoriamente la duda del usuario.", "catalog/open/habbiween"));
                else
                    Element.OtherElement.Session.SendMessage(RoomNotificationComposer.SendBubble("ambassador", "" + Session.GetHabbo().Username + ", gracias por colaborar en el programa de Alfas, has atendido satisfactoriamente la duda del usuario.", "catalog/open/habbiween"));
            }

            Element.Close();
        }
    }
}
