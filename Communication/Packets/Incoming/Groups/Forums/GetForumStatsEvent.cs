﻿using Cloud.Communication.Packets.Outgoing;
using Cloud.Communication.Packets.Outgoing.Groups;
using Cloud.Communication.Packets.Outgoing.Rooms.Notifications;
using Cloud.HabboHotel.GameClients;
using Cloud.HabboHotel.Groups.Forums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cloud.Communication.Packets.Incoming.Groups
{
    class GetForumStatsEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            var GroupForumId = Packet.PopInt();

            GroupForum Forum;
            if (!CloudServer.GetGame().GetGroupForumManager().TryGetForum(GroupForumId, out Forum))
            {
                CloudServer.GetGame().GetClientManager().SendMessage(RoomNotificationComposer.SendBubble("forums_thread_hidden", "El foro al que intentas acceder ya no existe.", ""));
                return;
            }

            Session.SendMessage(new ForumDataComposer(Forum, Session));

        }
    }
}
