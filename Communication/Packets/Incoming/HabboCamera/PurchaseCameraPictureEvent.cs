﻿using Cloud.Communication.Packets.Outgoing.HabboCamera;
using Cloud.Communication.Packets.Outgoing.Rooms.Notifications;
using Cloud.HabboHotel.GameClients;
using Cloud.HabboHotel.Items;
using Cloud.HabboHotel.Rooms.Camera;
using Cloud.Core;

namespace Cloud.Communication.Packets.Incoming.HabboCamera
{
    class PurchaseCameraPictureEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            int PictureBaseId = 202030;
            var conf = ExtraSettings.CAMERA_ITEMID;
            if (!int.TryParse(conf, out PictureBaseId))
            {
                Session.SendMessage(new RoomNotificationComposer("Por favor, hable con el equipo de desarrolladores que puto ID de la cámara de fotos antesalas no esta define en db.\n Disculpe la inconveniencia!", "error"));
                return;
            }
            CloudServer.GetGame().GetAchievementManager().ProgressAchievement(Session, "ACH_CameraPhotoCount", 1);
            var pic = HabboCameraManager.GetUserPurchasePic(Session);
            ItemData ibase = null;
            if (pic == null || !CloudServer.GetGame().GetItemManager().GetItem(PictureBaseId, out ibase))
                return;

            Session.GetHabbo().GetInventoryComponent().AddNewItem(0, ibase.Id, pic.Id.ToString(), 0, true, false, 0, 0);
            Session.GetHabbo().GetInventoryComponent().UpdateItems(false);
            
            Session.SendMessage(new CamereFinishPurchaseComposer());
        }
    }
}
