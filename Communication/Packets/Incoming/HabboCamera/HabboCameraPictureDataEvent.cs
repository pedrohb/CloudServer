﻿using Cloud.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cloud.Communication.Packets.Outgoing.Rooms.Notifications;
using Cloud.HabboHotel.Rooms.Camera;
using Cloud.Communication.Packets.Outgoing.HabboCamera;

namespace Cloud.Communication.Packets.Incoming.HabboCamera
{
    public class HabboCameraPictureDataEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            var len = Packet.PopInt();
            var bytes = Packet.ReadBytes(len);//Not in use when MOD camera
            
            HabboCameraManager.GetUserPurchasePic(Session, true);
            HabboCameraManager.AddNewPicture(Session);
        }
    }
}
