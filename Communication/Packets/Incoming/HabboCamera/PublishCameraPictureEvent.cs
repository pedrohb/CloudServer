﻿using Cloud.Communication.Packets.Outgoing.HabboCamera;
using Cloud.HabboHotel.GameClients;
using Cloud.HabboHotel.Rooms.Camera;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cloud.Communication.Packets.Incoming.HabboCamera
{
    class PublishCameraPictureEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            var pic = HabboCameraManager.GetUserPurchasePic(Session);
            if (pic == null)
                return;

            int InsetId;
            using (var Adap = CloudServer.GetDatabaseManager().GetQueryReactor())
            {
                Adap.SetQuery("INSERT INTO server_pictures_publish (picture_id) VALUES (@pic)");
                Adap.AddParameter("pic", pic.Id);
                InsetId = (int)Adap.InsertQuery();
            }

            Session.SendMessage(new CameraFinishPublishComposer(InsetId));

        }
    }
}
