﻿using Cloud.Utilities;
using Cloud.HabboHotel.Rooms;
using Cloud.HabboHotel.Moderation;

namespace Cloud.Communication.Packets.Outgoing.Moderation
{
    class ModeratorTicketChatlogComposer : ServerPacket
    {
        public ModeratorTicketChatlogComposer(ModerationTicket Ticket, RoomData RoomData, double Timestamp)
            : base(ServerPacketHeader.ModeratorTicketChatlogMessageComposer)
        {
            base.WriteInteger(Ticket.Id);
            base.WriteInteger(Ticket.Sender != null ? Ticket.Sender.Id : 0);
            base.WriteInteger(Ticket.Reported != null ? Ticket.Reported.Id : 0);
            base.WriteInteger(RoomData.Id);

            base.WriteByte(1);
            base.WriteShort(2);//Count
            base.WriteString("roomName");
            base.WriteByte(2);
            base.WriteString(RoomData.Name);
            base.WriteString("roomId");
            base.WriteByte(1);
            base.WriteInteger(RoomData.Id);

            base.WriteShort(Ticket.ReportedChats.Count);
            foreach (string Chat in Ticket.ReportedChats)
            {
                base.WriteString(UnixTimestamp.FromUnixTimestamp(Timestamp).ToShortTimeString());
                base.WriteInteger(Ticket.Id);
                base.WriteString(Ticket.Reported != null ? Ticket.Reported.Username : "No username");
                base.WriteString(Chat);
                base.WriteBoolean(false);
            }
        }
    }
}
