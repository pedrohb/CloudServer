﻿using Cloud.Utilities;
using Cloud.HabboHotel.Rooms;
using System.Collections.Generic;
using Cloud.HabboHotel.Rooms.Chat.Logs;

namespace Cloud.Communication.Packets.Outgoing.Moderation
{
    class ModeratorRoomChatlogComposer : ServerPacket
    {
        public ModeratorRoomChatlogComposer(Room Room, ICollection<ChatlogEntry> chats)
            : base(ServerPacketHeader.ModeratorRoomChatlogMessageComposer)
        {
            base.WriteByte(1);
            base.WriteShort(2);//Count
            base.WriteString("roomName");
            base.WriteByte(2);
            base.WriteString(Room.Name);
            base.WriteString("roomId");
            base.WriteByte(1);
            base.WriteInteger(Room.Id);

            base.WriteShort(chats.Count);
            foreach (ChatlogEntry Entry in chats)
            {
                string Username = "Unknown";
                if (Entry.PlayerNullable() != null)
                {
                    Username = Entry.PlayerNullable().Username;
                }

                base.WriteString(UnixTimestamp.FromUnixTimestamp(Entry.Timestamp).ToShortTimeString()); // time?
                base.WriteInteger(Entry.PlayerId); // User Id
                base.WriteString(Username); // Username
                base.WriteString(!string.IsNullOrEmpty(Entry.Message) ? Entry.Message : "** user sent a blank message **"); // Message        
                base.WriteBoolean(false); //TODO, AI's?
            }
        }
    }
}
