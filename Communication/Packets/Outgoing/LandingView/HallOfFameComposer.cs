﻿using Cloud.Communication.Packets.Incoming.LandingView;
using System.Collections.Generic;
using Cloud.HabboHotel.Cache.Type;

namespace Cloud.Communication.Packets.Outgoing.LandingView
{
    class HallOfFameComposer : ServerPacket
    {
        public HallOfFameComposer() : base(ServerPacketHeader.UpdateHallOfFameListMessageComposer)
        {
            base.WriteString("halloffame.staff");
            GetHallOfFame.getInstance().Serialize(this);
            return;
        }
    }
}
