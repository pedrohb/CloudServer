﻿using System.Collections.Generic;
using Cloud.HabboHotel.Groups;

namespace Cloud.Communication.Packets.Outgoing.Catalog
{
    class GroupFurniConfigComposer : ServerPacket
    {
        public GroupFurniConfigComposer(ICollection<Group> Groups)
            : base(ServerPacketHeader.GroupFurniConfigMessageComposer)
        {
            base.WriteInteger(Groups.Count);
            foreach (Group Group in Groups)
            {
                base.WriteInteger(Group.Id);
                base.WriteString(Group.Name);
                base.WriteString(Group.Badge);
                base.WriteString(CloudServer.GetGame().GetGroupManager().GetColourCode(Group.Colour1, true));
                base.WriteString(CloudServer.GetGame().GetGroupManager().GetColourCode(Group.Colour2, false));
                base.WriteBoolean(false);
                base.WriteInteger(Group.CreatorId);
                base.WriteBoolean(Group.ForumEnabled);
            }
        }
    }
}
