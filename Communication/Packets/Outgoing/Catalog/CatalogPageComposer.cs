﻿using System;
using System.Linq;

using Cloud.Core;
using Cloud.HabboHotel.Items;
using Cloud.HabboHotel.Catalog;
using Cloud.HabboHotel.Items.Utilities;
using Cloud.HabboHotel.Catalog.Utilities;

namespace Cloud.Communication.Packets.Outgoing.Catalog
{
    public class CatalogPageComposer : ServerPacket
    {
        public CatalogPageComposer(CatalogPage Page, string CataMode)
            : base(ServerPacketHeader.CatalogPageMessageComposer)
        {
            base.WriteInteger(Page.Id);
            base.WriteString(CataMode);
            base.WriteString(Page.Template);

            base.WriteInteger(Page.PageStrings1.Count);
            foreach (string s in Page.PageStrings1)
            {
                base.WriteString(s);
            }

            base.WriteInteger(Page.PageStrings2.Count);
            foreach (string s in Page.PageStrings2)
            {
                base.WriteString(s);
            }

            if (!Page.Template.Equals("frontpage") && !Page.Template.Equals("loyalty_vip_buy"))
            {
                base.WriteInteger(Page.Items.Count);
                foreach (CatalogItem Item in Page.Items.Values)
                {
                    base.WriteInteger(Item.Id);
                    base.WriteString(Item.Name);
                    base.WriteBoolean(false);//IsRentable
                    base.WriteInteger(Item.CostCredits);

                    if (Item.CostDiamonds > 0)
                    {
                        base.WriteInteger(Item.CostDiamonds);
                        base.WriteInteger(5); // Diamonds
                    }
                    else if (Item.CostPixels > 0)
                    {
                        base.WriteInteger(Item.CostPixels);
                        base.WriteInteger(0); // Type of PixelCost

                    }
                    else
                    {
                        base.WriteInteger(Item.CostGotw);
                        base.WriteInteger(103); // Gotw
                    }

                    base.WriteBoolean(Item.PredesignedId > 0 ? false : ItemUtility.CanGiftItem(Item));
                    if (Item.PredesignedId > 0)
                    {
                        base.WriteInteger(Page.PredesignedItems.Items.Count);
                        foreach (var predesigned in Page.PredesignedItems.Items.ToList())
                        {
                            ItemData Data = null;
                            if (CloudServer.GetGame().GetItemManager().GetItem(predesigned.Key, out Data)) { }
                            base.WriteString(Data.Type.ToString());
                            base.WriteInteger(Data.SpriteId);
                            base.WriteString(string.Empty);
                            base.WriteInteger(predesigned.Value);
                            base.WriteBoolean(false);
                        }

                        base.WriteInteger(0);
                        base.WriteBoolean(false);
                        base.WriteBoolean(true); // Niu Rilí
                        base.WriteString(""); // Niu Rilí
                    }
                    else if (Page.Deals.Count > 0)
                    {
                        foreach (var Deal in Page.Deals.Values)
                        {
                            base.WriteInteger(Deal.ItemDataList.Count);
                            foreach (var DealItem in Deal.ItemDataList.ToList())
                            {
                                base.WriteString(DealItem.Data.Type.ToString());
                                base.WriteInteger(DealItem.Data.SpriteId);
                                base.WriteString(string.Empty);
                                base.WriteInteger(DealItem.Amount);
                                base.WriteBoolean(false);
                            }

                            base.WriteInteger(0);
                            base.WriteBoolean(false);
                        }
                    }
                    else
                    {
                        base.WriteInteger(string.IsNullOrEmpty(Item.Badge) ? 1 : 2);//Count 1 item if there is no badge, otherwise count as 2.
                        {
                            if (!string.IsNullOrEmpty(Item.Badge))
                            {
                                base.WriteString("b");
                                base.WriteString(Item.Badge);
                            }

                            base.WriteString(Item.Data.Type.ToString());
                            if (Item.Data.Type.ToString().ToLower() == "b")
                            {
                                //This is just a badge, append the name.
                                base.WriteString(Item.Data.ItemName);
                            }
                            else
                            {
                                base.WriteInteger(Item.Data.SpriteId);
                                if (Item.Data.InteractionType == InteractionType.WALLPAPER || Item.Data.InteractionType == InteractionType.FLOOR || Item.Data.InteractionType == InteractionType.LANDSCAPE)
                                {
                                    base.WriteString(Item.Name.Split('_')[2]);
                                }
                                else if (Item.Data.InteractionType == InteractionType.BOT)//Bots
                                {
                                    CatalogBot CatalogBot = null;
                                    if (!CloudServer.GetGame().GetCatalog().TryGetBot(Item.ItemId, out CatalogBot))
                                        base.WriteString("hd-180-7.ea-1406-62.ch-210-1321.hr-831-49.ca-1813-62.sh-295-1321.lg-285-92");
                                    else
                                        base.WriteString(CatalogBot.Figure);
                                }
                                else if (Item.ExtraData != null)
                                {
                                    base.WriteString(Item.ExtraData != null ? Item.ExtraData : string.Empty);
                                }
                                base.WriteInteger(Item.Amount);
                                base.WriteBoolean(Item.IsLimited); // IsLimited
                                if (Item.IsLimited)
                                {
                                    base.WriteInteger(Item.LimitedEditionStack);
                                    base.WriteInteger(Item.LimitedEditionStack - Item.LimitedEditionSells);
                                }
                            }
                            base.WriteInteger(0); //club_level
                            base.WriteBoolean(ItemUtility.CanSelectAmount(Item));

                            base.WriteBoolean(true); // Niu Rilí
                            base.WriteString(""); // Niu Rilí
                        }
                    }
                }
                /*}*/
            }
            else
                base.WriteInteger(0);
            base.WriteInteger(-1);
            base.WriteBoolean(false);
            if (Page.Template.Equals("frontpage4"))
            {
                base.WriteInteger(4); // count

                //New Rare Jellyfish Lamp!
                base.WriteInteger(1); // id
                base.WriteString(CatalogSettings.CATALOG_NOTICE_1); // name
                base.WriteString(CatalogSettings.CATALOG_IMG_NOTICE_1); // image
                base.WriteInteger(0);
                base.WriteString(CatalogSettings.CATALOG_URL_NOTICE_1); // page link?
                base.WriteInteger(-1); // page id?


                base.WriteInteger(2);
                base.WriteString(CatalogSettings.CATALOG_NOTICE_2); // name
                base.WriteString(CatalogSettings.CATALOG_IMG_NOTICE_2); // image
                base.WriteInteger(0);
                base.WriteString(CatalogSettings.CATALOG_URL_NOTICE_2); // page link?
                base.WriteInteger(-1);


                base.WriteInteger(3);
                base.WriteString(CatalogSettings.CATALOG_NOTICE_3); // name
                base.WriteString(CatalogSettings.CATALOG_IMG_NOTICE_3); // image
                base.WriteInteger(0);
                base.WriteString(CatalogSettings.CATALOG_URL_NOTICE_3); // page link?
                base.WriteInteger(-1);


                base.WriteInteger(4);
                base.WriteString(CatalogSettings.CATALOG_NOTICE_4); // name
                base.WriteString(CatalogSettings.CATALOG_IMG_NOTICE_4); // image
                base.WriteInteger(0);
                base.WriteString(CatalogSettings.CATALOG_URL_NOTICE_4); // page link?
                base.WriteInteger(-1);

                if (Page.Template.Equals("loyalty_vip_buy"))
                {
                    base.WriteInteger(0); //Page ID
                    base.WriteString("NORMAL");
                    base.WriteString("loyalty_vip_buy");
                    base.WriteInteger(2);
                    base.WriteString("hc2_clubtitle");
                    base.WriteString("clubcat_pic");
                    base.WriteInteger(0); // Nueva Release
                    base.WriteInteger(0);
                    base.WriteInteger(-1);
                    base.WriteBoolean(false);

                    if (Page.Template.Equals("club_gifts"))
                    {
                        base.WriteString("club_gifts");
                        base.WriteInteger(1);
                        base.WriteString(Convert.ToString(Page.PageStrings2));
                        base.WriteInteger(1);
                        base.WriteString(Convert.ToString(Page.PageStrings2));
                    }
                }
            }
        }
    }
}