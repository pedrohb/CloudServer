﻿namespace Cloud.Communication.Packets.Outgoing.Catalog
{
    public class RecyclerStateComposer : ServerPacket
    {
        public RecyclerStateComposer(int ItemId = 0)
            : base(ServerPacketHeader.RecyclerStateComposer)
        {
            base.WriteInteger(1);
            base.WriteInteger(ItemId);
        }
    }
}