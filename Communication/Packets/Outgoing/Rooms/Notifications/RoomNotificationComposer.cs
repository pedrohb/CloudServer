﻿using System;
using System.Collections.Generic;

namespace Cloud.Communication.Packets.Outgoing.Rooms.Notifications
{
    internal class RoomNotificationComposer : ServerPacket
    {
        public RoomNotificationComposer(string Type, string Key, string Value) : base(ServerPacketHeader.RoomNotificationMessageComposer)
        {
            base.WriteString(Type);
            base.WriteInteger(1);
            base.WriteString(Key);
            base.WriteString(Value);
        }

        public RoomNotificationComposer(string Type) : base(ServerPacketHeader.RoomNotificationMessageComposer)
        {
            base.WriteString(Type);
            base.WriteInteger(0);
        }

        public RoomNotificationComposer(string Title, string Message, string Image, string HotelName = "", string HotelURL = "", bool isBubble = false) : base(ServerPacketHeader.RoomNotificationMessageComposer)
        {
            base.WriteString(Image);
            base.WriteInteger(5);
            base.WriteString("title");
            base.WriteString(Title);
            base.WriteString("message");
            base.WriteString(Message);
            base.WriteString("linkUrl");
            base.WriteString(HotelURL);
            base.WriteString("linkTitle");
            base.WriteString(HotelName);
            base.WriteString("display");
            base.WriteString(isBubble ? "BUBBLE" : "POP_UP");
        }

        public RoomNotificationComposer(string Type, Dictionary<string, string> Keys) : base(ServerPacketHeader.RoomNotificationMessageComposer)
        {
            base.WriteString(Type);
            base.WriteInteger(Keys.Count);
            foreach (KeyValuePair<string, string> current in Keys)
            {
                base.WriteString(current.Key);
                base.WriteString(current.Value);
            }
        }

        public static ServerPacket SendBubble(string image, string message, string linkUrl = "")
        {
            var bubbleNotification = new ServerPacket(ServerPacketHeader.RoomNotificationMessageComposer);
            bubbleNotification.WriteString(image);
            bubbleNotification.WriteInteger(string.IsNullOrEmpty(linkUrl) ? 2 : 3);
            bubbleNotification.WriteString("display");
            bubbleNotification.WriteString("BUBBLE");
            bubbleNotification.WriteString("message");
            bubbleNotification.WriteString(message);
            if (string.IsNullOrEmpty(linkUrl)) return bubbleNotification;
            bubbleNotification.WriteString("linkUrl");
            bubbleNotification.WriteString(linkUrl);
            return bubbleNotification;
        }

        public static ServerPacket SendCustom(string Message)
        {
            var cuz = new ServerPacket(ServerPacketHeader.RoomNotificationMessageComposer);

            cuz.WriteInteger(1);
            cuz.WriteString(Message);

            return cuz;
        }

        public RoomNotificationComposer(string Text, string Image) : base(ServerPacketHeader.RoomNotificationMessageComposer)
        {
            base.WriteString(Image);
            base.WriteInteger(2);
            base.WriteString("message");
            base.WriteString(Text);
            base.WriteString("display");
            base.WriteString("BUBBLE");
        }

        public RoomNotificationComposer(string image, int messageType, string message, string link)
            : base(ServerPacketHeader.RoomNotificationMessageComposer)
        {
            base.WriteString(image);
            base.WriteInteger(messageType);
            base.WriteString("display");
            base.WriteString("BUBBLE");
            base.WriteString("message");
            base.WriteString(message);
            base.WriteString("linkUrl");
            base.WriteString(link);

        }
    }
}
