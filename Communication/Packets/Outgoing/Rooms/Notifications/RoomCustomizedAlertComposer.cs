﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Cloud.Communication.Packets.Outgoing.Rooms.Notifications
{
    class RoomAlertComposer : ServerPacket
    {
        public RoomAlertComposer(string Message)
            : base(ServerPacketHeader.RoomAlertComposer)

        {
            base.WriteInteger(1);
            base.WriteString(Message);
        }
    }
}