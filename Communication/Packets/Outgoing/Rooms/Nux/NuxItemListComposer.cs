﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Cloud.Database.Interfaces;

namespace Cloud.Communication.Packets.Outgoing.Rooms.Nux
{
    class NuxItemListComposer : ServerPacket
    {
        public NuxItemListComposer() : base(ServerPacketHeader.NuxItemListComposer)
        {
            base.WriteInteger(1); // Número de páginas.

            base.WriteInteger(1); // ELEMENTO 1
            base.WriteInteger(3); // ELEMENTO 2
            base.WriteInteger(3); // Número total de premios:

            using (IQueryAdapter dbQuery = CloudServer.GetDatabaseManager().GetQueryReactor())
            {
                dbQuery.SetQuery("SELECT * FROM `nux_gifts` LIMIT 3");
                DataTable gUsersTable = dbQuery.getTable();

                foreach (DataRow Row in gUsersTable.Rows)
                {
                    base.WriteString(Convert.ToString(Row["image"])); // image.library.url + string
                    base.WriteInteger(1); // items:
                    base.WriteString(Convert.ToString(Row["title"])); // item_name (product_x_name)
                    base.WriteString(""); // can be null
                }
            }

            #region Eybuenas xd
            //base.WriteInteger(1); // Número de páginas.

            //base.WriteInteger(1); // ELEMENTO 1
            //base.WriteInteger(3); // ELEMENTO 2
            //base.WriteInteger(2); // Número total de premios:

            //base.WriteString("nux/hc_promotion.png"); // image.library.url + string
            //base.WriteInteger(1); // items:
            //base.WriteString("a0 throne"); // item_name (product_x_name)
            //base.WriteString(""); // can be null

            //base.WriteString("nux/xxxx.png");
            //base.WriteInteger(1);
            //base.WriteString("a0 spyro");
            //base.WriteString("");


            /*  base.WriteInteger(0);
              base.WriteInteger(0);
              base.WriteInteger(3);
              base.WriteString("nux/hc_promotion.png"); // Imagen en SWFs
              base.WriteInteger(1);
              base.WriteString("hc_promotion"); // Nombre Premio
              base.WriteString("");
              base.WriteString("nux/xxxx.png");
              base.WriteInteger(1);
              base.WriteString("nux_gift_diamonds");
              base.WriteString("");
              base.WriteString("nux/xxxx.png");
              base.WriteInteger(1);
              base.WriteString("nux_gift_vip_1_day");
              base.WriteString(""); */

            //base.WriteInteger(1); // Número de páginas.

            //base.WriteInteger(1);
            //base.WriteInteger(1);
            //base.WriteInteger(1);
            //base.WriteString("nux/hc_promotion.png"); // Imagen en SWFs
            //base.WriteInteger(1);
            //base.WriteString("hc_promotion"); // Nombre Premio
            //base.WriteString("");
            //base.WriteString("nux/xxxx.png");
            //base.WriteInteger(2);
            //base.WriteString("nux_gift_diamonds");
            //base.WriteString("");
            //base.WriteString("nux/xxxx.png");
            //base.WriteInteger(3);
            //base.WriteString("nux_gift_vip_1_day");
            //base.WriteString("");
            #endregion
        }
    }
}