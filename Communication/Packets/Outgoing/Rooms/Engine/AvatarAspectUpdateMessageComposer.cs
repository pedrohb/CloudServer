﻿using System;
using System.Linq;
using System.Text;

using Cloud.HabboHotel.Rooms;
using Cloud.HabboHotel.GameClients;

namespace Cloud.Communication.Packets.Outgoing.Rooms.Engine
{
    class AvatarAspectUpdateMessageComposer : ServerPacket
    {
        public AvatarAspectUpdateMessageComposer(string Figure, string Gender)
            : base(ServerPacketHeader.AvatarAspectUpdateMessageComposer)
        {
            base.WriteString(Figure);
            base.WriteString(Gender);

        }
    }
}