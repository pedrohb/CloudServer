﻿namespace Cloud.Communication.Packets.Outgoing.Navigator
{
    class NavigatorPreferencesComposer : ServerPacket
    {
        public NavigatorPreferencesComposer()
            : base(ServerPacketHeader.NavigatorPreferencesMessageComposer)
        {
			base.WriteInteger(68);
			base.WriteInteger(42);
            base.WriteInteger(425);//Width
            base.WriteInteger(592);//Height
            base.WriteBoolean(false);
			base.WriteInteger(0);
        }
    }
}

