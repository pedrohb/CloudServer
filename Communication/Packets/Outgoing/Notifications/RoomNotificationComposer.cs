﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

namespace Cloud.Communication.Packets.Outgoing.Notifications
{
    class NewRoomNotifComposer : ServerPacket
    {
        public NewRoomNotifComposer(string Type, string Key, string Value)
            : base(ServerPacketHeader.RoomNotificationMessageComposer)
        {
            base.WriteString(Type);
            base.WriteInteger(1);//Count
            base.WriteString(Key);//Type of message
            base.WriteString(Value);
        }
    }
}
