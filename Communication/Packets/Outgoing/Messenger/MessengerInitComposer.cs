﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Cloud.HabboHotel.Users;
using Cloud.HabboHotel.Users.Messenger;
using Cloud.HabboHotel.Users.Relationships;

namespace Cloud.Communication.Packets.Outgoing.Messenger
{
    class MessengerInitComposer : ServerPacket
    {
        public MessengerInitComposer(HabboHotel.GameClients.GameClient Session)
            : base(ServerPacketHeader.MessengerInitMessageComposer)
        {
            base.WriteInteger(Convert.ToInt32(CloudServer.GetGame().GetSettingsManager().TryGetValue("messenger.buddy_limit")));//Friends max.
            base.WriteInteger(300);
            base.WriteInteger(800);
            base.WriteInteger(1); // category count
            base.WriteInteger(1);
            base.WriteString("Grupos");
        }
    }
}
