﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Cloud.HabboHotel.Achievements;

namespace Cloud.Communication.Packets.Outgoing.Inventory.Achievements
{
    class BadgeDefinitionsComposer: ServerPacket
    {
        public BadgeDefinitionsComposer(Dictionary<string, Achievement> Achievements)
            : base(ServerPacketHeader.BadgeDefinitionsMessageComposer)
        {
            base.WriteInteger(Achievements.Count);

            foreach (Achievement Achievement in Achievements.Values)
            {
               base.WriteString(Achievement.GroupName.Replace("ACH_", ""));
                base.WriteInteger(Achievement.Levels.Count);
                for (int i = 1; i < Achievement.Levels.Count + 1; i++)
                {
                    base.WriteInteger(i);
                    base.WriteInteger(Achievement.Levels[i].Requirement);
                }
            }
            base.WriteInteger(0);
        }
    }
}
