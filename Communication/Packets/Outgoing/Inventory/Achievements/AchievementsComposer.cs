﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

using Cloud.HabboHotel.GameClients;
using Cloud.HabboHotel.Achievements;

namespace Cloud.Communication.Packets.Outgoing.Inventory.Achievements
{
    class AchievementsComposer : ServerPacket
    {
        public AchievementsComposer(GameClient Session, List<Achievement> Achievements)
            : base(ServerPacketHeader.AchievementsMessageComposer)
        {
            base.WriteInteger(Achievements.Count);
            foreach (Achievement Achievement in Achievements)
            {
                UserAchievement UserData = Session.GetHabbo().GetAchievementData(Achievement.GroupName);
                int TargetLevel = (UserData != null ? UserData.Level + 1 : 1);
                int TotalLevels = Achievement.Levels.Count;

                TargetLevel = (TargetLevel > TotalLevels ? TotalLevels : TargetLevel);
                int i = UserData != null ? (UserData.Level + 1) : 1;

                int count = Achievement.Levels.Count;
                if (i > count)
                    i = count;
                AchievementLevel TargetLevelData = Achievement.Levels[TargetLevel];
                AchievementLevel achievementLevel = Achievement.Levels[i];
                AchievementLevel oldLevel = (Achievement.Levels.ContainsKey(i - 1)) ? Achievement.Levels[i - 1] : achievementLevel;
                base.WriteInteger(Achievement.Id); // Unknown (ID?)
                base.WriteInteger(i); // Target level
                base.WriteString(string.Format("{0}{1}", Achievement.GroupName, i)); // Target name/desc/badge
                base.WriteInteger(oldLevel.Requirement);
                base.WriteInteger(TargetLevelData.Requirement); // Progress req/target          
                base.WriteInteger(TargetLevelData.RewardPixels);
                base.WriteInteger(0); // Type of reward
                base.WriteInteger(UserData != null ? UserData.Progress : 0); // Current progress
                if (UserData == null)
                    base.WriteBoolean(false);
                else if (UserData.Level >= TotalLevels)
                    base.WriteBoolean(true);
                else
                    base.WriteBoolean(false);
                base.WriteString(Achievement.Category); // Category
                base.WriteString(string.Empty);
                base.WriteInteger(count); // Total amount of levels 
                base.WriteInteger(0);
            }
           base.WriteString("");
        }
    }
}