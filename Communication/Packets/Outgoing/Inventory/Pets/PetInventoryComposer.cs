﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Cloud.HabboHotel.Rooms.AI;
using Cloud.HabboHotel.GameClients;
using Cloud.HabboHotel.Users.Inventory;

namespace Cloud.Communication.Packets.Outgoing.Inventory.Pets
{
    class PetInventoryComposer : ServerPacket
    {
        public PetInventoryComposer(ICollection<Pet> Pets)
            : base(ServerPacketHeader.PetInventoryMessageComposer)
        {
            base.WriteInteger(1);
            base.WriteInteger(1);
            base.WriteInteger(Pets.Count);
            foreach (Pet Pet in Pets.ToList())
            {
                base.WriteInteger(Pet.PetId);
                base.WriteString(Pet.Name);
                base.WriteInteger(Pet.Type);
                base.WriteInteger(int.Parse(Pet.Race));
                base.WriteString(Pet.Color);
                base.WriteInteger(0);
                if (Pet.Type == 15)
                {
                    base.WriteInteger(4);
                    base.WriteInteger(1);
                    base.WriteInteger(-1);
                    base.WriteInteger(int.Parse(Pet.Race));
                    base.WriteInteger(2);
                    base.WriteInteger(Pet.PetHair);
                    base.WriteInteger(Pet.HairDye);
                    base.WriteInteger(3);
                    base.WriteInteger(Pet.PetHair);
                    base.WriteInteger(Pet.HairDye);
                    base.WriteInteger(4);
                    base.WriteInteger(Pet.Saddle);
                    base.WriteInteger(0);
                }
                else
                    base.WriteInteger(0);
                base.WriteInteger(0);
            }
        }
    }
}