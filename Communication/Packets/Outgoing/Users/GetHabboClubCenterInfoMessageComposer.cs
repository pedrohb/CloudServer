﻿using System;
using Cloud.HabboHotel.GameClients;

namespace Cloud.Communication.Packets.Outgoing.Inventory.Purse
{
    class GetHabboClubCenterInfoMessageComposer : ServerPacket
    {
        public GetHabboClubCenterInfoMessageComposer(GameClient Session) : base(ServerPacketHeader.HabboClubCenterInfoMessageComposer)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(Session.GetHabbo().GetClubManager().GetSubscription("habbo_vip").ActivateTime);
            base.WriteInteger(2005);//streakduration in days 
            if (Session.GetHabbo().GetClubManager().HasSubscription("habbo_vip"))
                base.WriteString(origin.ToString("dd/MM/yyyy hh:mm:ss tt"));//joindate 
            else
                base.WriteString("No ha sido activada todavia");
            base.WriteInteger(0);
            base.WriteInteger(0);//this should be a double 
            base.WriteInteger(0);//unused 
            base.WriteInteger(0);//unused 
            base.WriteInteger(10);//spentcredits 
            base.WriteInteger(20);//streakbonus 
            base.WriteInteger(10);//spentcredits 
            base.WriteInteger(60);//next pay in minutes
        }
    }
}