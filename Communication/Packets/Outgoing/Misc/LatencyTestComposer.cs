﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Cloud.HabboHotel.GameClients;

namespace Cloud.Communication.Packets.Outgoing.Misc
{
    class LatencyTestComposer : ServerPacket
    {
        public LatencyTestComposer(GameClient Session, int testResponce)
            : base(ServerPacketHeader.LatencyResponseMessageComposer)
        {
            if (Session == null)
                return;

            Session.TimePingedReceived = DateTime.Now;

            base.WriteInteger(testResponce);
            CloudServer.GetGame().GetAchievementManager().ProgressAchievement(Session, "ACH_AllTimeHotelPresence", 1);
        }
    }
}
